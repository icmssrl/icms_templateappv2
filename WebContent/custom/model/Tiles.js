jQuery.sap.declare("model.Tiles");
jQuery.sap.require("model.i18n");


model.Tiles = function () {



  TileCollection = [
    {
      "icon": "create-session",
      //"title": model.i18n.getText("CUSTOMER_MANAGEMENT"),
      "title": model.i18n._getLocaleText("CUSTOMER_MANAGEMENT"),
      "url": "customersList",
      "type": "None",
      "isAgent": true,
      "inSession": false
		},
    {
      "icon": "customer",
      //"title": model.i18n.getText("CREATE_NEW_CUSTOMER"),
      "title": model.i18n._getLocaleText("CREATE_NEW_CUSTOMER"),
      "url": "newCustomer",
      "type": "None",
      "isAgent": true,
      "inSession": false
		},
      
//    {
//      "icon": "customer-history",
//      "title": model.i18n.getText("CUSTOMER_SYNCRO"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},
    {
      "icon": "customer-order-entry",
      //"title": model.i18n.getText("ORDER_LIST"),
      "title": model.i18n._getLocaleText("ORDER_LIST"),
      "url": "orderList",
      "type": "None",
      "isAgent": true,
      "inSession": false
		},
//    {
//      "icon": "citizen-connect",
//      "title": model.i18n.getText("FAIR"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},
//    {
//      "icon": "table-chart",
//      "title": model.i18n.getText("EXCEL_LIST"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},
//    {
//      "icon": "feed",
//      "title": model.i18n.getText("FEED"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},
//    {
//      "icon": "locked",
//      "title": model.i18n.getText("CHANGE_PASSWORD"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": false
//		},
    //tile di sessione
    {
      "icon": "add",
      //"title": model.i18n.getText("NEW_ORDER_CREATE"),
      "title": model.i18n._getLocaleText("NEW_ORDER_CREATE"),
      "url": "newOrder",
      "type": "None",
      "isAgent": true,
      "inSession": true
		},
    {
      "icon": "lead-outdated",
      //"title": model.i18n.getText("CATALOG"),
      "title": model.i18n._getLocaleText("CATALOG"),
      "url": "emptyROHierarchy",
      "type": "None",
      "isAgent": true,
      "inSession": true
		},
      {
      "icon": "history",
      //"title": model.i18n.getText("HISTORY_CARTS"),
      "title": model.i18n._getLocaleText("HISTORY_CARTS"),
      "url": "emptyHistoryCarts",
      "type": "None",
      "isAgent": true,
      "inSession": true
		},
    {
      "icon": "customer-order-entry",
      //"title": model.i18n.getText("ORDER_LIST"),
      "title": model.i18n._getLocaleText("ORDER_LIST"),
      "url": "orderList",
      "type": "None",
      "isAgent": true,
      "inSession": true
		},
//    {
//      "icon": "sales-order-item",
//      "title": model.i18n.getText("ORDER_ACTIVE_VIEW"),
//      //this.getView().getModel("i18n").getResourceBundle().getText("ORDER_ACTIVE_VIEW"),
//      //this.getView().getModel("i18n").getText("ORDER_ACTIVE_VIEW")
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": true
//		},
//    {
//      "icon": "globe",
//      "title": model.i18n.getText("DESTINATIONS"),
//      "url": "",
//      "type": "None",
//      "isAgent": true,
//      "inSession": true
//		},
    {
      "icon": "customer-briefing",
      //"title": model.i18n.getText("CUSTOMER_REPORT"),
      "title": model.i18n._getLocaleText("CUSTOMER_REPORT"),
      "url": "",
      "type": "None",
      "isAgent": true,
      "inSession": true
		},
    {
      "icon": "log",
      //"title": model.i18n.getText("CLOSE_SESSION"),
      "title": model.i18n._getLocaleText("CLOSE_SESSION"),
      "url": "launchpad",
      "type": "None",
      "isAgent": true,
      "inSession": true
		}

  ];

  divisionTiles = [
    {
      "textColor":"lightgreen",
      "backgroundColor":"white",
      "icon": "./custom/img/loghi/beretta.gif",
      "title": "Beretta",
      "url": "login",
      "division": "BR"
    },
    {
      "textColor":"red",
      "backgroundColor":"white",
      "icon": "./custom/img/loghi/riello.gif",
      "title": "Riello Trade",
      "url": "launchpad",
      "division": "RI"
    },
    {
      
      "icon": "",
      "title": "" ,
      "url": "login",
      "division": "SM"
    },
    {
    
      "icon": "",
      "title": "" ,
      "url": "login",
      "division":"SY"
    },
    {
        
      "icon": "",
      "title": "",
      "url": "login",
      "division":"TH"
    },
    {
       
      "icon": "",
      "title": "" ,
      "url": "login",
      "division":"FO"
    },
    {
        
      "icon": "",
      "title":"" ,
      "url": "login",
      "division":"BD"
    },
    {
        
      "icon": "",
      "title":"" ,
      "url": "login",
      "division":"CF"
    }

  ];

  //To integrate when userType entity is realized
//  var map = [{"userType": "agent","tiles":[{"id":1}, {"id":2}, {"id":3}]}];

  //getById:function(idCode)
  //{
      //retun _.find(TileCollection, {id:idCode});
//}
// var userTiles = _.where(map, {userType : type});
  // var tiles = _.map(userTiles, this.getById(item.id));
//  if (isCustomerSession) {
    // arrayTiles = _.where(tiles, {
    //   'inSession': true
    // });

  getTiles = function (type, isCustomerSession) {


    if (type.toUpperCase() === "Agent".toUpperCase()) {
      if (isCustomerSession) {
        arrayTiles = _.where(TileCollection, {
          'isAgent': true,
          'inSession': true
        });
      } else {
        var arrayTiles = _.where(TileCollection, {
          'isAgent': true,
          'inSession': false
        });
      }
    } else {
      //toDO
    }
    return arrayTiles;
  };

  getDivisionTile = function(division)
  {
    return _.find(divisionTiles, {division : division});
  };

  return {
    getMenu: getTiles,
    getDivisionTile: getDivisionTile
    
  };



}();
