jQuery.sap.declare("model.Order");
jQuery.sap.require("model.collections.Destinations");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.Position");


model.Order = (function () {

  Order = function (serializedData) {



      this.orderId = undefined;
      this.customerId = undefined;
      this.customerName = undefined;
      this.rifOrder = undefined;
      this.basketType = undefined;
      this.destination = undefined;
      this.paymentMethod = undefined;
      this.paymentCondition = undefined;
      this.resa1 = undefined;
      this.resa2 = undefined;
      this.meansShipping = undefined;
      this.totalEvasion = undefined;
      this.appointmentToDelivery = undefined;
      this.deliveryType = undefined;
      this.chargeTrasport = undefined;
      this.IVACode = undefined;

      this.validDateList = undefined;
      this.requestedDate = undefined;
      this.orderReason = undefined;
      this.positions = [];
      this.billsNotes = [];
      this.salesNotes = [];



    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };

    this.create = function (customerId) {
      var defer = Q.defer();
      var fSuccess = function (res) {
        if (res) {
          this.customerId = res.getId();
          this.customerName = res.getCustomerName();

        }
        defer.resolve(res);
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function (err) {
        console.log("Error loading Customer Order Data");
        defer.reject(err);
      }
      fError = _.bind(fError, this);

      model.collections.Customers.getById(customerId)
        .then(fSuccess, fError);

      return defer.promise;

    };

    this.getPositions = function () {
      if (!this.positions || this.positions.length === 0)
        this.positions = [];
      return this.positions;
    };
    this.addPosition = function (position) {
      // var row = new model.Position(position);
      var row = position;
      if (!this.positions || this.positions.length === 0)
        this.positions = [];
      this.positions.push(row);
    };

    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };



    if (serializedData) {
      this.update(serializedData);
    }

    this.getId = function () {
      return this.orderId;
    };


    this.removePosition = function (position) {
      if (this.positions.length > 0) {
        for (var i = 0; i < this.positions.length; i++) {
          if (position === this.positions[i].positionId) {
            this.positions.splice(i, 1);
            break;
          }
        }
      }
      return;

    };

    // this.setDestination = function (destinationCode)
    // {
    //   if(destinationCode)
    //   {
    //     var d = model.collections.Destinations.getById(destinationCode);//Maybe to correct, it depends from customer's destinations
    //     this.destination = d.destination;
    //   }
    //   else
    //   {
    //     //mi prendo l'id del primo elemento dell'array delle destinazioni di questo customer
    //     var currentCustomer = model.collections.Customers.getById(this.customerCode);
    //     var destinations = currentCustomer.getDestination();
    //     if(destinations && destinations.length>0)
    //     {
    //       this.destination = destinations[0].destination;
    //     }
    //   }
    // };
    this.setDestination = function (destination) {
      this.destination = destination;
    };

    this.addNote = function(param, oEntry)
    {
      var note = {"text":oEntry.text, "author": oEntry.author};
      this[param][0] = note;
    };


    return this;
  };

  return Order;


})();
