jQuery.sap.declare("model.User");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("utils.Busy");

model.User = ( function (){

  // _username = "" ;
  // _password = "" ;
  // _type = "";
  // _id = " ";

  User  = function(data)
  {
    this.username = "";
    this.password  ="";
    this.fullName = "";
    this.customer = "";
    this.name1="";
    this.mail ="";
    this.type = "agent";
    this.organizationData =[];

    this.setCredential = function(mail, pwd)
    {
      this.mail = mail;
      this.password = pwd;
    };

    // this.getCredential = function()
    // {
    //   return {"username": this.username, "password": this.password};
    // };
    this.getCredential = function()
    {
      return {"username": this.mail, "password": this.password};
    };

    this.update=function(data)
    {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

    this.setUserInfo = function()
    {
      var defer = Q.defer();
      var fSuccess = function(result)
      {
        result = _.map(result, _.bind(function(item)
        {
          return model.persistence.Serializer.user.fromSAP(item);
        }, this));

        var userInfo = _.find(result, { mail: this.mail });
        if(!userInfo)
          defer.reject("userInfo not found");
        else
        {
          // var data = model.persistence.Serializer.user.fromSAP(userInfo);
          this.update(userInfo);
          defer.resolve(userInfo);
          console.log("userInfo");


          // model.persistence.Storage.session.save("user", user);
          // defer.resolve(user);
        }
      };
      fSuccess = _.bind(fSuccess, this);

      var fError = function(err)
      {
        defer.reject(err);
      };
      fError = _.bind(fError, this);

      $.getJSON("custom/model/mock/data/userInfo.json")
        .success(fSuccess)
        .fail(fError);


      return defer.promise;



    };

    // this.getNewCredential = function()
    // {
    //   this.username = "";
    //   this.password = "";
    //   return new sap.ui.model.json.JSONModel({"username": this.username, "password": this.password});
    // };

    this.getNewCredential = function()
    {
      this.mail = "";
      this.password = "";
      return new sap.ui.model.json.JSONModel({"username": this.mail, "password": this.password});
    };

    this.getModel = function()
    {
      return new sap.ui.model.json.JSONModel(this);

    };


    this.doLogin = function()
    {

        var loginDefer = Q.defer();
        utils.Busy.show();

        var fSuccess = function(result)
        {
          utils.Busy.hide();
          model.persistence.Storage.session.save("user", this);
          loginDefer.resolve(this);
        };
        fSuccess = _.bind(fSuccess, this);

        var fError  = function(err)
        {
          utils.Busy.hide();
          loginDefer.reject(err);
        };
        fError = _.bind(fError, this);


        var checkCredential = function()
        {
          var defer = Q.defer();
          var fSuccess = function(result)
          {


            // var user = _.find(result, { username: this.username , password : this.password});
            var user = _.find(result, { username: this.mail , password : this.password});

            if(!user){
              sap.m.MessageToast.show("user not found");

              defer.reject("user not found");
            }
            else
            {

              defer.resolve(user);
              console.log("userInLogin");
              console.log(user);

              // model.persistence.Storage.session.save("user", user);
              // defer.resolve(user);
            }
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/users.json")
            .success(fSuccess)
            .fail(fError);


          return defer.promise;

        };
        checkCredential = _.bind(checkCredential, this);




        checkCredential()
        .then(_.bind(this.setUserInfo, this))
          .then(fSuccess, fError);


  //---------------To use when linked to SAP/HanaDB--------------------//


        //   var tok = username + ':' + password;
        // var hash = btoa(tok);
        // var auth = "Basic " + hash;
        //
        // console.log(hash);
        // console.log(auth);
        //
        // $.ajaxSetup({
        // 	headers: {
        // 		"Authorization": auth
        // 	}
        // });
          // var url = setting.Core.serverUrl + "ZFIORI_SRV/$metadata"
          //
          // $.ajax({
          // 	url: url,
          // 	type: "GET", //or POST?
          // 	// dataType: "jsonp",
          // 	xhrFields: {
          // 		withCredentials: true
          // 	},
          // 	beforeSend: function (request) {
          // 		request.setRequestHeader("Authorization", auth);
          // 	},
          // 	success: success,
          // 	error: error
          // });
  //-------------------------------------------------------------------------------//

        return loginDefer.promise;
      };

      if(data)
        this.update(data);

      return this;



  };

  return User;








// return {
//
//   setCurrentUser: function(userInfo)
//   {
//     _username = userInfo.username ;
//     _password = userInfo.password ;
//     _type = userInfo.type;
//
//   },
//   getCurrentUser: function()
//   {
//     return this;
//   },
//   getUsername : function()
//   {
//     return this._username;
//   },
//   setUserName : function(value)
//   {
//     this._username = value;
//   },
//   setPassword : function(value)
//   {
//     this._password  = value;
//   },
//
//   getNewCredential : function()
//   {
//     this._username = "";
//     this._password = "";
//     return this.getModel();
//   },
//
//   getModel:function()
//   {
//     var model = new sap.ui.model.json.JSONModel();
//     var user = this.getCurrentUser();
//     model.setData(user);
//     return model;
//   },
//
//   doLogin : function()
//   {
//
//       var defer = Q.defer();
//       utils.Busy.show();
//
//       var fSuccess = function(result)
//       {
//         utils.Busy.hide();
//         var user = _.find(result, { username: this._username , password : this._password});
//         if(!user)
//           defer.reject("user not found");
//         else {
//           this.setCurrentUser(user);
//           console.log("userInLogin");
//           console.log(user);
//
//           model.persistence.Storage.session.save("user", user);
//           defer.resolve(user);
//         }
//       };
//       var fError = function(err)
//       {
//         utils.Busy.hide();
//         defer.reject(err);
//       };
//       fSuccess = _.bind(fSuccess, this);
//       fError = _.bind(fError,this);
//
//       $.getJSON("custom/model/mock/data/users.json")
//         .success(fSuccess)
//         .fail(fError);
//
// //---------------To use when linked to SAP/HanaDB--------------------//
//
//
//       //   var tok = username + ':' + password;
// 			// var hash = btoa(tok);
// 			// var auth = "Basic " + hash;
//       //
// 			// console.log(hash);
// 			// console.log(auth);
//       //
// 			// $.ajaxSetup({
// 			// 	headers: {
// 			// 		"Authorization": auth
// 			// 	}
// 			// });
//         // var url = setting.Core.serverUrl + "ZFIORI_SRV/$metadata"
//         //
//   			// $.ajax({
//   			// 	url: url,
//   			// 	type: "GET", //or POST?
//   			// 	// dataType: "jsonp",
//   			// 	xhrFields: {
//   			// 		withCredentials: true
//   			// 	},
//   			// 	beforeSend: function (request) {
//   			// 		request.setRequestHeader("Authorization", auth);
//   			// 	},
//   			// 	success: success,
//   			// 	error: error
//   			// });
// //-------------------------------------------------------------------------------//
//
//       return defer.promise;
//     },
//
//     sessionSave : function()
//     {
//       var user = {username : this._username, type: this._type};
//       model.persistence.Storage.session.save("user", user );
//     }
//
//
//
//
//
//   };

  })();
