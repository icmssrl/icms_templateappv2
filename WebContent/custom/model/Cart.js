jQuery.sap.declare("model.Cart");

model.Cart = ( function () {

  Cart = function(data)
  {
    this.orderId  = undefined;
    this.customerId = undefined;
    this.positions = [];

    this.update = function(data)
    {
      for(var prop in data)
      {
        this[prop]= data[prop];
      }
    };

    this.create = function(orderId, customerId)
    {
      this.orderId= orderId;
      this.customerId = customerId;
    };

    this.addPosition = function(position)
    {
      if(!this.positions || this.positions.length === 0)
        this.positions = [];

      this.positions.push(position);
    };
    this.setPositions = function(positionsArr)
    {
      if(positionsArr && positionsArr.length > 0)
      {
        this.positions = positionsArr;
      }
    };
    this.getPositions = function()
    {
      if(!this.positions || this.positions.length === 0)
        this.positions = [];

      return this.positions;
    };
    this.getModel = function()
    {
      var model = new sap.ui.model.json.JSONModel(this);
      return model;
    };


  if(data)
    this.update(data);

  return this;
}

  return Cart;


})();
