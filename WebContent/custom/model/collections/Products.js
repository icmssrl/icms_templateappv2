
jQuery.sap.declare("model.collections.Products");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Product");


model.collections.Products = ( function ()
{
  var products = [];
  //var defer = Q.defer();

  return {

    getById : function(code)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var product = _.find(result, _.bind(function(item)
        {
          return item.getId() === code;
        }, this));
        if(product)
        {
          deferId.resolve(product); //
        }
        else
        {
          console.log("Product.js -- Product not found!");
        }
        
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      };
      
      fError = _.bind(fError, this);

      this.loadAllProducts()
      .then(fSuccess, fError);

      return deferId.promise;
    },



    //Maybe to move in a odataClass in the future

    loadAllProducts : function()
    {
        var defer = Q.defer();
        
        if(defer && defer.promise.isFulfilled())
        {
          defer.resolve(products);
        }
        else
        {
          var fSuccess = function(result)
          {
            if(result && result.length>0)
            {
              products=[];
              var add = _.bind(this.addProduct, this);
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.product.fromSAP(result[i]);
                add(new model.Product(data));
              }

            }
            defer.resolve(products);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            products  = [];
            console.log("Error loading Products");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/products2.json")
            .success(fSuccess)
            .fail(fError);
        }
        // defer = Q.defer();



        return defer.promise;
    },
      
     loadProducts : function(lvl2Id)
    {
        var defer = Q.defer();
        
//        if(defer && defer.promise.isFulfilled())
//        {
//          defer.resolve(products);
//        }
//        else
//        {
        
          var fSuccess = function(result)
          {
            if(result && result.length>0)
            {
              products=[];
              var add = _.bind(this.addProduct, this);
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.product.fromSAP(result[i]);
                if(data.parentId === lvl2Id)
                  add(new model.Product(data));
              }

            }
            defer.resolve(products);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            products  = [];
            console.log("Error loading Products");
            defer.reject(err);
          };
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/products2.json")
            .success(fSuccess)
            .fail(fError);
//        }




        return defer.promise;
    }, 
      
    addProduct : function(product)
    {
      products.push(product);
    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"products":[]};
      for(var i = 0; i< products.length; i++)
      {
        data.products.push(products[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
