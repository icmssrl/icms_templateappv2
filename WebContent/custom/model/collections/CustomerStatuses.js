jQuery.sap.declare("model.collections.CustomerStatuses");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Customer");



model.collections.CustomerStatuses = ( function ()
{
  var customerStatuses = [];
  var defer = Q.defer();

  return {

    getById : function(id)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var customerStatus = _.find(result, _.bind(function(item)
        {
          return item.getId() === id;
        }, this));
        deferId.resolve(customerStatus);
      }
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      }
      fError = _.bind(fError, this);

      this.loadCustomerStatuses()
      .then(fSuccess, fError);

      return deferId.promise;
    },



    //Maybe to move in a odataClass in the future

    loadCustomerStatuses : function(forceReload)//In case of an added customer , force reload list cause it could return additional info
    {
        if(defer && defer.promise.isFulfilled() && !forceReload)
        {
          defer.resolve(customerStatuses);
        }
        else
        {
          defer = Q.defer();
          var fSuccess = function(result)
          {
            var add = _.bind(this.addCustomerStatuses, this);
            if(result && result.length>0)
            {
              customerStatuses=[];
              for(var i = 0; i <result.length; i++)
              {
                var data = model.persistence.Serializer.customerStatus.fromSAP(result[i]);
                add(new model.CustomerStatus(data));
              }

            }
            defer.resolve(customerStatuses);
          }
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            customerStatuses  = [];
            console.log("Error loading CustomerStatuses");
            defer.reject(err);
          }
          fError = _.bind(fError, this);

          $.getJSON("custom/model/mock/data/customerStatuses.json")
            .success(fSuccess)
            .fail(fError);
        }
        // defer = Q.defer();



        return defer.promise;
    },
    //--------------------------------------------------------------------

    addCustomerStatuses : function (customerStatus)
    {
      customerStatuses.push(customerStatus);
    },

    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"customerStatuses":[]};
      for(var i = 0; i< customerStatuses.length; i++)
      {
        data.customerStatuses.push(customerStatuses[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  }
})();
