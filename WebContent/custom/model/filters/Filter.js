jQuery.sap.declare("model.filters.Filter");
jQuery.sap.require("utils.ObjectUtils");
jQuery.sap.require("model.i18n");

model.filters.Filter = {

//the property 'collection' is used to refer the elements' type on which apply the filter

  // getModel : function(list, collection)
  // {
  //   if(!list || list.length==0)
  //     return;
  //
  //   // It's taken an object of the list as a template to capture object's properties
  //   //--> it's supposed every list object has the same property,
  //   // eventually loop on every object and fill an array of properties props
  //
  //   var template = list[0];
  //   var props = utils.ObjectUtils.getKeys(template);
  //
  //   //Check exist filterData
  //   if(!this.checkFilterExistence(collection))
  //   {
  //     this.filterData[collection] = {"items":[]};
  //   }
  //
  //   // var item = {"propertyName": "", "values": []};
  //   // var valueItem = {"key": "" , "prop" : "", "isSelected": false};
  //
  //   if(!props || props.length === 0 )
  //     return;
  //
  //
  //     _.forEach(props,
  //       _.bind(function(prop)
  //       {
  //         var item = {"propertyName": prop, "values": []};
  //         var values=[];
  //       _.map(list, function(obj)
  //         {
  //           var propName = prop.substring(prop.indexOf("/"), prop.length-1);
  //           values.push({"value": utils.ObjectUtils.getValues(obj, prop), "property": prop, "isSelected": false});
  //         });
  //         item.values=_.uniq(values, 'value');
  //         this.filterData[collection].items.push(item);
  //       }, this));
  //
  //   console.log(this.filterData);
  //
  //
  // },
  checkFilterExistence: function(collection)
  {
    return (this.filterData && this.filterData[collection] && this.filterData[collection].items && this.filterData[collection].items.length > 0);

  },

  //To implement in the getModel function
  // updateFilter:function(list, collection)
  // {
  //
  //
  //
  // },

  deleteFilter:function(collection)
  {
    this.filterData[collection] = undefined;
  },
  resetFilter:function(collection)
  {
    var selectedItems = this.getSelectedItems(collection);
    for(var i = 0; i< selectedItems.length; i++)
    {
      selectedItems[i].isSelected=false;
    }
    return;
  },
  getFilterData:function(collection)
  {
    if(!this.checkFilterExistence(collection))
      return null;
    return this.filterData[collection];

  },
  getSelectedItems: function(collection)
  {
    var filterItems = [];
    if(!this.checkFilterExistence(collection))
    {
      return null;
    }
    var items = this.filterData[collection].items;
		for(var i = 0 ; i< items.length; i++)
		{
			for(var j=0; j< items[i].values.length; j++)
			{
				if(items[i].values[j].isSelected)
				{
					filterItems.push(items[i].values[j]);
				}
			}
		}
		return filterItems;
  },

  getModel:function(list, collection)
  {
    if(!list || list.length==0)
      return;


    if(!this.checkFilterExistence(collection))
    {
      this.filterData={};
      this.filterData[collection] = {"items":[]};
      // this.props[collection] = [];
    }

    _.forEach(list,  _.bind(
      function (item)
      {
        var itemProps = utils.ObjectUtils.getKeys(item);

        if(!itemProps || itemProps.length == 0)
          return;

        _.forEach(itemProps, _.bind(
          function(collection, prop)
          {
            var filterPropertyValue = utils.ObjectUtils.getValues(item, prop);
            this.addFilterItem(prop, filterPropertyValue, collection)

          },this, collection));


      }, this));


    console.log(this.filterData[collection]);
    return new sap.ui.model.json.JSONModel(this.filterData[collection]);
  },

  addFilterItem: function(prop, value, collection)
  {
    if(!this.checkFilterExistence(collection))
    {
      this.filterData[collection].items=[];
    }
    var parentItem = _.find(this.filterData[collection].items, {'propertyName' : prop});
    if(!parentItem)
    {
      parentItem = {"propertyName": prop, "title": this._getTitle(prop),"values":[]};
      this.filterData[collection].items.push(parentItem);
    }
    if(!_.find(parentItem.values, {"value": value}))
    {
      parentItem.values.push({"value":value, "property": prop, "isSelected": false});
    }
    return;

  },
  _getTitle:function(prop)
  {
    if(prop.indexOf("/") !== -1){
        var barIdx = prop.lastIndexOf("/");
    }
    if(barIdx && barIdx < 0)
    {
      return prop;
    }
    var text = prop.substring(barIdx+1, prop.length).toUpperCase();
    if(prop.indexOf("/") !== barIdx)
    {
      text = prop.substring(0, prop.indexOf("/")) + text;
    }


    return model.i18n._getLocaleText(text.toUpperCase());
  }



}
