jQuery.sap.declare("model.Customer");
jQuery.sap.require("model.Destination");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.CustomerStatus");
jQuery.sap.require("model.collections.CustomerStatuses");

model.Customer = (function () {




  Customer = function (serializedData) {


    this.registry = {};
    this.contact = {};
    this.bank = {};
    this.sales = {};
    this.destinations = [];
    this.customerStatus = {};

    this.initialize = function(user, division)
    {
      this.registry.userName = user.username;
      this.registry.division = division;
    }
    // initialize = function () {
    //
    //   this.registry = {
    //     id: undefined,
    //     registryType: undefined,
    //     companyName: undefined,
    //     VATNumber: undefined,
    //     street: undefined,
    //     numAddr: undefined,
    //     postalCode: undefined,
    //     city: undefined,
    //     prov: undefined,
    //     nation: undefined
    //   };
    //
    //   this.destinations=[];
    //
    //
    //   this.contact = {
    //     phone: undefined,
    //     mobile: undefined,
    //     fax: undefined,
    //     contactType: undefined,
    //     mail: undefined
    //   };
    //
    //   this.bank = {
    //     iban: undefined,
    //     descr: undefined,
    //     bankNation: undefined,
    //     accountNum: undefined,
    //     abcab: undefined,
    //     cin: undefined
    //   };
    //
    //   this.sales = {
    //     clientType: undefined,
    //     billType: undefined,
    //     billFreq: undefined,
    //     paymentCond: undefined,
    //     resa: undefined,
    //     incoterms2: undefined,
    //     carrier: undefined,
    //     transport: undefined,
    //     notes: undefined,
    //   };
    // };

    this.setRegistry = function (id, registryType, companyName, customerName, VATNumber, street, numAddr, postalCode, city, prov, nation, taxCode) {
      this.registry.id = id;
      this.registry.registryType = registryType;
      this.registry.customerName = customerName;
      this.registry.companyName = companyName;
      this.registry.VATNumber = VATNumber;
      this.registry.street = street;
      this.registry.numAddr = numAddr;
      this.registry.postalCode = postalCode;
      this.registry.city = city;
      this.registry.prov = prov;
      this.registry.nation = nation;
      this.registry.taxCode = taxCode;
    };

    this.addDestination = function(dest)
    {
     this.destinations.push(dest);
    };

    this.getDestinations = function()
    {
      return this.destinations;
    };



    this.setContact = function (phone, mobile, fax, contactType, mail) {
      this.contact.phone = phone;
      this.contact.mobile = mobile;
      this.contact.fax = fax;
      this.contact.contactType = contactType;
      this.contact.mail = mail;
    };


    this.setBank = function (iban, descr, nation, accountNum, abcab, cin) {
      this.bank.iban = iban;
      this.bank.descr = descr;
      this.bank.bankNation = nation;
      this.bank.accountNum = accountNum;
      this.bank.abcab = abcab;
      this.bank.cin = cin;
    };


    this.setSales = function (clientType, billType, billFreq, paymentCond, reso, incoterms2, carrier, transport, notes) {
      this.sales.clientType = clientType;
      this.sales.billType = billType;
      this.sales.billFreq = billFreq;
      this.sales.paymentCond = paymentCond;
      this.sales.resa = reso;
      this.sales.incoterms2 = incoterms2;
      this.sales.carrier = carrier;
      this.sales.transport = transport;
      this.sales.notes = notes;
    };



    this.getAddress = function () {
      return this.registry;
    };
    this.getContact = function () {
      return this.contact;
    };

    this.getSales = function () {
      return this.sales;
    };
    this.getBank = function () {
      return this.bank;
    };

    this.getId = function () {
      return this.registry.id;
    };

    this.getCompanyName = function() {
      return this.registry.companyName;
    };
    
    this.getCustomerName = function() {
      return this.registry.customerName;
    };

    this.getDestinations = function()
    {
      return this.destinations;
    };

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);

      return model;

    };


    this.setCustomerStatus = function(customerStatus)
    {

        model.collections.CustomerStatuses.getById(this.getId())
          .then(_.bind(function(result){this.customerStatus = result;},this));
    };
    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }

      // //addDestination
      if(!this.destinations || this.destinations.length === 0)
      {
        this.addDestination(model.persistence.Serializer.destination.fromCustomer(this));
      }
      if(!this.customerStatus)
      {
        this.customerStatus = new model.CustomerStatus();
      }

    };

    // initialize();

//To review first instantation
    if (serializedData) {

      this.update(serializedData);




    }
    // if(!this.destinations || this.destinations.length === 0)
    // {
    //   this.destinations = [];
    //   if(this.registry)
    //   {
    //     this.destinations.push(_.cloneDeep(this.registry));
    //   }
    //
    // }



    return this;
  };
  return Customer;


})();
