jQuery.sap.declare("model.persistence.Serializer");

model.persistence.Serializer = {

  customer: {
    fromSAP: function (sapData) {
      var c = {};
      //------------------From customer.json--------------------------
      // c.registry = {};
      // c.registry.id = sapData.id ? sapData.id : "";
      // c.registry.registryType = sapData.registryType ? sapData.registryType : "";
      // c.registry.companyName = sapData.companyName ? sapData.companyName : "";
      // c.registry.VATNumber = sapData.VATNumber ? sapData.VATNumber : "";
      // c.registry.street = sapData.street ? sapData.street : "";
      // c.registry.numAddr = sapData.numAddr ? sapData.numAddr : "";
      // c.registry.postalCode = sapData.postalCode ? sapData.postalCode : "";
      // c.registry.city = sapData.city ? sapData.city : "";
      // c.registry.prov = sapData.prov ? sapData.prov : "";
      // c.registry.nation = sapData.nation ? sapData.nation : "";
      //
      // c.sales = {};
      // c.sales.clientType = sapData.clientType ? sapData.clientType : "";
      // c.sales.billType = sapData.billType ? sapData.billType : "";
      // c.sales.billFreq = sapData.billFreq ? sapData.billFreq : "";
      // c.sales.paymentCond = sapData.paymentCond ? sapData.paymentCond : "";
      // c.sales.resa = sapData.resa ? sapData.resa : "";
      // c.sales.incoterms2 = sapData.incoterms2 ? sapData.incoterms2 : "";
      // c.sales.carrier = sapData.carrier ? sapData.carrier : "";
      // c.sales.transport = sapData.transport ? sapData.transport : "";
      // c.sales.notes = sapData.notes ? sapData.notes : "";
      //
      // c.bank = {};
      // c.bank.iban = sapData.iban ? sapData.iban : "";
      // c.bank.descr = sapData.descr ? sapData.descr : "";
      // c.bank.bankNation = sapData.bankNation ? sapData.bankNation : "";
      // c.bank.accountNum = sapData.accountNum ? sapData.accountNum : "";
      // c.bank.abcab = sapData.abcab ? sapData.abcab : "";
      // c.bank.cin = sapData.cin ? sapData.cin : "";
      //
      // c.contact = {};
      // c.contact.phone = sapData.phone ? sapData.phone : "";
      // c.contact.mobile = sapData.mobile ? sapData.mobile : "";
      // c.contact.fax = sapData.fax ? sapData.fax : "";
      // c.contact.contactType = sapData.type ? sapData.contactType : "";
      // c.contact.mail = sapData.mail ? sapData.mail : "";
      //-----------------------------------------------------------------------
      //----From customer2.json--------------------------------------
      c.registry = {};
      c.registry.id = sapData.customer ? sapData.customer : "";

      c.registry.taxCode = sapData.codiceFiscale ? sapData.codiceFiscale:"";
      c.registry.customerName = sapData.customerName?sapData.customerName:"";
      c.registry.userName = sapData.username?sapData.username:"";

      c.registry.division = sapData.division?sapData.division:"";
      c.registry.divisionName = sapData.divisionName?sap.divisionName:"";

      c.registry.salesOrg = sapData.salesOrg? sapData.salesOrg:"";
      c.registry.salesOrgName = sapData.salesOrgName? sapData.salesOrgName:"";



      c.registry.registryType = sapData.registryType ? sapData.registryType : "";
      c.registry.companyName = sapData.companyName ? sapData.companyName : "";
      c.registry.VATNumber = sapData.partitaIva ? sapData.partitaIva : "";
      c.registry.street = sapData.address ? sapData.address : "";
      c.registry.numAddr = sapData.numAddr ? sapData.numAddr : "";
      c.registry.postalCode = sapData.postZone ? sapData.postZone : "";
      c.registry.city = sapData.city ? sapData.city : "";
      c.registry.prov = sapData.regionName ? sapData.regionName : "";
      c.registry.region = sapData.region ? sapData.region : "";
      c.registry.nation = sapData.landName ? sapData.landName : "";
      c.registry.land = sapData.land ? sapData.land : "";

      c.sales = {};
      c.sales.clientType = sapData.clientType ? sapData.clientType : "S.T.A/C.A.T";
      c.sales.billType = sapData.billType ? sapData.billType : "";
      c.sales.billFreq = sapData.billFreq ? sapData.billFreq : "";
      c.sales.paymentCond = sapData.paymentCond ? sapData.paymentCond : "";
      c.sales.resa = sapData.resa ? sapData.resa : "";
      c.sales.incoterms2 = sapData.incoterms2 ? sapData.incoterms2 : "";
      c.sales.carrier = sapData.carrier ? sapData.carrier : "";
      c.sales.transport = sapData.transport ? sapData.transport : "";
      c.sales.notes = sapData.notes ? sapData.notes : "";

      c.bank = {};
      c.bank.iban = sapData.iban ? sapData.iban : "";
      c.bank.descr = sapData.descr ? sapData.descr : "";
      c.bank.bankNation = sapData.bankNation ? sapData.bankNation : "";
      c.bank.accountNum = sapData.accountNum ? sapData.accountNum : "";
      c.bank.abcab = sapData.abcab ? sapData.abcab : "";
      c.bank.cin = sapData.cin ? sapData.cin : "";

      c.contact = {};
      c.contact.phone = sapData.phone ? sapData.phone : "";
      c.contact.mobile = sapData.mobile ? sapData.mobile : "";
      c.contact.fax = sapData.fax ? sapData.fax : "";
      c.contact.contactType = sapData.type ? sapData.contactType : "";
      c.contact.mail = sapData.mail ? sapData.mail : "";
      //----------------------------------------------------------------------
      return c;

    },
    toSAP: {

    }

  },

  customerStatus:{
    fromSAP: function (sapData) {
      var cs = {};
      cs.customerId = sapData.customerId ? sapData.customerId : 0;
      cs.totalDebt  = sapData.totalDebt ? sapData.totalDebt : 0;
      cs.totalOrder = sapData.totalOrder ? sapData.totalOrder : 0;
      cs.usedDebt = sapData.usedDebt ? sapData.usedDebt : 0;
      cs.payedOrder = sapData.payedOrder ? sapData.payedOrder : 0;
      return cs;
    },
    toSap: function(data)
    {

    }
  },

  destination: {
    fromSAP: function (sapData) {
      var d = {};

      d.address = sapData.address;
      d.city = sapData.city;
      d.company = sapData.company;
      d.country = sapData.country;
      //d.country_text = sapData.country_text;
      d.customer = sapData.kunnr;
      d.destination = sapData.destination; //codice destinazione
      //d.destination_text = sapData.destination_text;
      //d.distribution_channel = sapData.distribution_channel;
      //d.division = sapData.division;
      d.email = sapData.email;
      d.fax = sapData.fax;
      d.postal_code = sapData.postal_code;
      d.region = sapData.region;
      //d.region_text = sapData.region_text;
      //d.sales_organization = sapData.sales_organization;
      d.telephone = sapData.telephone;

      return d;
    },
    fromCustomer:function(customerData) {
      var d = {};

      d.address = customerData.registry.street+", "+customerData.registry.numAddr;
      d.city = customerData.registry.city;
      d.company = customerData.registry.companyName;
      d.country = customerData.registry.nation;
      //d.country_text = sapData.country_text;
      d.customer = customerData.registry.id;
      d.destination = "" //codice destinazione
      //d.destination_text = sapData.destination_text;
      //d.distribution_channel = sapData.distribution_channel;
      //d.division = sapData.division;
      d.email = customerData.contact.mail;
      d.fax = customerData.contact.fax;
      d.postal_code = customerData.registry.postalCode;
      d.region = customerData.registry.prov;
      //d.region_text = sapData.region_text;
      //d.sales_organization = sapData.sales_organization;
      d.telephone = customerData.contact.phone;
      return d;

    },
    toSAP: function (d) {
      var toSapData = {};
      return toSapData;
    }
  },

  discount:
  {
    fromSAP:function(sapData)
    {
      var d = {};
      d.productId = sapData.productId;
      // d.price.unitVal = sapData.price;
      
      d.agentDiscount = {};
      d.agentDiscount.unitVal = sapData.agentDiscount;
      d.firstLocDiscount = {};
      d.firstLocDiscount.unitVal = sapData.firstLocDiscount;
      d.secondLocDiscount={};
      d.secondLocDiscount.unitVal = sapData.secondLocDiscount;
      d.allegedCommRC={};
      d.allegedCommRC.unitVal = sapData.allegedCommRC;
      d.addTraspRC={};
      d.addTraspRC.unitVal = sapData.addTraspRC;
      d.addTraspAutomRC={};
      d.addTraspAutomRC.unitVal = sapData.addTraspAutomRC;
      d.IVA={};
      d.IVA.unitVal = sapData.IVA;
      d.thirdLocDiscount={};
      d.thirdLocDiscount.unitVal = sapData.thirdLocDiscount;
      d.currency = sapData.currency;
      return d;
    }

  },
    
  hierarchyNode:
  {
    fromSAP: function self(sapData){
      var n = {};
      n.productId = sapData.productId ? sapData.productId : "";
      n.salesOrg = sapData.salesOrg ? sapData.salesOrg : "";
      n.distrCh = sapData.distrCh ? sapData.distrCh : "";
      n.division = sapData.division ? sapData.division : "";
      n.description = sapData.description ? sapData.description : "";
      n.parentId = sapData.parentId ? sapData.parentId : "";
      n.level = sapData.level ? sapData.level : "";
      n.productPicUrl = sapData.productPicUrl ? sapData.productPicUrl : "";
      n.items = [];
      if(sapData.items && sapData.items.length > 0)
      {
        for(var i = 0 ; i<sapData.items.length; i++)
        {
          //Maybe to correct
          sapData.items[i].parentId = n.productId;
          n.items.push(self(sapData.items[i]));
        }
      }
      return n;
    },


  },
    
  order:
  {
    fromSAP: function (sapData) {
      var o = {};
      o.orderId = sapData.orderId;
      o.customerId = sapData.customerId;
      o.companyName = sapData.companyName;
      o.rifOrder = sapData.rifOrder;
      o.basketType = sapData.basketType;
      o.destination = sapData.destination;
      o.paymentMethod = sapData.paymentMethod;
      o.paymentCondition = sapData.paymentCondition;
      o.resa1 = sapData.resa1;
      o.resa2 = sapData.resa2;
      o.meansShipping = sapData.meansShipping;
      o.totalEvasion = sapData.totalEvasion;
      o.appointmentToDelivery = sapData.appointmentToDelivery;
      o.deliveryType = sapData.deliveryType;
      o.chargeTrasport = sapData.chargeTrasport;
      o.IVACode = sapData.IVACode;
      o.validateDateList = sapData.validateDateList;
      o.requestedDate = sapData.requestedDate;
      o.orderReason = sapData.orderReason;
      var tmpItems = model.persistence.order_item.fromSAPItems(sapData.positions);
      o.positions = tmpItems.items;
      o.billNote = sapData.billNote;
      o.salesNote = sapData.salesNote;

      return o;
    },

    toSAP: function (o) {
      var toSapData = {};
      return toSapData;
    }
  },
    
  historyCart:
  {
    fromSAP: function (sapData) {
      var o = {};
      o.orderId = sapData.orderId;
      o.customerId = sapData.customerId;
      o.customerName = sapData.customerName;
      o.rifOrder = sapData.rifOrder;
      o.basketType = sapData.basketType;
      o.destination = sapData.destination;
      o.paymentMethod = sapData.paymentMethod;
      o.paymentCondition = sapData.paymentCondition;
      o.resa1 = sapData.resa1;
      o.resa2 = sapData.resa2;
      o.meansShipping = sapData.meansShipping;
      o.totalEvasion = sapData.totalEvasion;
      o.appointmentToDelivery = sapData.appointmentToDelivery;
      o.deliveryType = sapData.deliveryType;
      o.chargeTrasport = sapData.chargeTrasport;
      o.IVACode = sapData.IVACode;
      o.validateDateList = sapData.validateDateList;
      o.requestedDate = sapData.requestedDate;
      o.orderReason = sapData.orderReason;
      //var tmpItems = model.persistence.Serializer.order_item.fromSAPItems(sapData.positions);
      o.positions = sapData.positions;
      o.billNote = sapData.billNote;
      o.salesNote = sapData.salesNote;
      o.favorite = sapData.favorite ? sapData.favorite : false;
      o.fullEvasionAvailableDate = sapData.fullEvasionAvailableDate;

      return o;
    },

    toSAP: function (o) {
      var toSapData = {};
      return toSapData;
    }
  },

  order_item :
  {
    toSAPItems: function (items) {
      var ret = {
        Items: []
      };
      for (var i = 0; i < items.length; i++) {
        var oItem = this.toSap(items[i]);
        ret.Items.push(oItem);
      }
      return ret;
    },

    fromSAPItems : function(results)
    {
      var ret = {
        items: []
      };
      if (results.hasOwnProperty("results")) {
        var l = results.results.length;
        for (var i = 0; i < l; i++) {
          var oItem = this.fromSAP(results.results[i]);
          if (oItem !== undefined) {
            ret.items.push(oItem);
          }
        }
      }
      return ret;
    },

    fromSAP: function (sapData){

      var oi = {};
      oi.orderId = sapData.orderId;
      oi.positionId = sapData.positionId;
      oi.quantity = sapData.quantity;
      oi.totalListPrice = sapData.totalListPrice;
      oi.totalNetPrice = sapData.totalNetPrice;
      oi.discountApplyed = sapData.discountApplyed;
      oi.productId = sapData.productId;
      oi.scale = sapData.scale;
      oi.description = sapData.description;
      oi.unitListPrice  = sapData.unitListPrice;
      oi.unitNetPrice  = sapData.unitNetPrice;

      return oi;
    },
    toSAP : function(oi){}
  },

  product: {
    fromSAP: function (sapData) {
      var p = {};
      p.productId = sapData.productId ? sapData.productId : "";
      p.productPicUrl = sapData.productPicUrl ? sapData.productPicUrl : ""; //we can set a default icon??
      p.scale = sapData.scale ? sapData.scale : "";
      p.description=sapData.description ? sapData.description : "";
      p.parentId =sapData.parentId ? sapData.parentId : "";
      //p.description = sapData.description ? sapData.description : "";
      p.unitListPrice = parseFloat(sapData.unitListPrice) ? sapData.unitListPrice : 0;
      //p.unitNetPrice = parseFloat(sapData.unitNetPrice) ? sapData.unitNetPrice : 0;

      return p;
    },
    toSAP: function (p) {
      var toSapData = {};
      toSapData.Matnr = p.productId;
      toSapData.scale = p.scale;
      toSapData.Maktg = p.description;
      toSapData.unitListPrice = p.unitListPrice;
      //toSapData.unitNetPrice = p.unitNetPrice;

      return toSapData;
    }
  },

  user:{
    fromSAP: function(sapData)
    {
      var u = {};
      u.username = sapData.userGeneralData.username;
      u.fullName =  sapData.userGeneralData.fullName;
      u.customer =  sapData.userGeneralData.customer;
      u.name1= sapData.userGeneralData.name;
      u.mail = sapData.userGeneralData.email;

      if(sapData.organizationData && sapData.organizationData.results.length>0)
      {
        u.organizationData = [];
        var orgData =sapData.organizationData.results;
        for(var i = 0; i< orgData.length; i++)
        {
          u.organizationData.push(orgData[i]);
        }
      }
      return u;
    }
  }




};
