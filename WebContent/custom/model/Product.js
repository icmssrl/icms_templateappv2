jQuery.sap.declare("model.Product");

model.Product = (function () {

  Product = function (serializedData) {
    
    //proprietà dell'oggetto prodotto
    this.productId = undefined;
    //this.id = undefined;
    this.scale = undefined;
    this.description = undefined;
    //this.name = undefined;
    this.unitListPrice = undefined;
    this.unitNetPrice = undefined;
    this.productPicUrl = undefined;

    this.getId = function () {
      return this.productId;
    };

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };

    this.getUnitListPrice = function()
    {
      return this.unitListPrice;
    };

    this.getUnitNetPrice = function()
    {
      return this.unitNetPrice;
    };

    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

    if (serializedData) {
      this.update(serializedData);
    }
    return this;
  };
  return Product;


})();
