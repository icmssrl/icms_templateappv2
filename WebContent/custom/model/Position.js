jQuery.sap.declare("model.Position");
jQuery.sap.require("model.Product");
jQuery.sap.require("model.Order");

model.Position = (function () {

  Position = function (serializedData) {


    initialize = function () {

      this.order = undefined;
      this.orderId = undefined;
      this.positionId = undefined;
      this.quantity = undefined;
      this.totalListPrice = undefined;
      this.totalNetPrice = undefined;
      this.discountApplied = undefined;

      //aggiungere le proprietà del prodotto
      this.product = undefined;
      this.productId = undefined;
      this.scale = undefined;
      this.description = undefined;
      this.unitListPrice = undefined;
      this.unitNetPrice = undefined;

    };


//Yet to see if it makes any sense

    this.create = function (order, product) {
      this.setProduct(product);
      this.setOrder(order);

      this.quantity = 0;
      this.totalListPrice = 0;
      this.discountApplyed = 0;
    };

    this.totalNetPrice = function () {
      //TODO
    };

    this.calculateTotalListPrice = function () {

      this.totalListPrice= this.unitListPrice * this.quantity;

      return this.totalListPrice;
    };

    //maybe private
    this.setProduct = function (product) {

      // var p = model.collections.Product.getById(productId);
      this.product = product;

  //Maybe to delete and get this properties directly from product
      this.productId = product.productId;
      this.scale = product.scale;
      this.description = product.description;
      this.unitListPrice = product.unitListPrice;
      this.unitNetPrice = product.unitNetPrice;
        // for (var prop in p) {
        //   this[prop] = p[prop];
        // }

      };

    //Maybe private
    this.setOrder=function(order)
    {
      // this.order= order;
      this.orderId= order.getId();
        this.positionId = order.getPositions().length;
    };

    this.setDiscount = function(discount)
    {
      this.discount = discount;
    };

    this.setQuantity = function(quantity)
    {
      this.quantity = quantity;
    };

    this.setWantedDate = function(date)
    {
      this.wantedDate = new Date(date);
      this.calculateTotalListPrice();
    };

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };

    this.getDiscount=function(){

      return this.discount;
    };
    this.getQuantity=function()
    {
      if(!this.quantity)
        this.quantity=0;
      return this.quantity;
    };
    this.getProduct= function(){

      return this.product;
    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

    initialize();

    this.getProductId = function () {
      return this.productId;
    };

    this.getId = function () {
      return this.positionId;
    };

    if (serializedData) {
      this.update(serializedData);
    }

    return this;
  };
  return Position;


})();
