jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");

view.abstract.AbstractController.extend("view.CustomerEdit", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "customerEdit" ){
			return;
		}

		var id = evt.getParameters().arguments.id;
    this.openCustomerDetail(id);

//		model.collections.Customers.getById(id)
//		.then(
//
//			_.bind(function(result)
//			{
//				this.customer = result;
//				// this.refreshView(result, routeName)
//				this.refreshView(result);
//			}, this)
//		);

	},
	// refreshView : function(data, route)
	// {
	// 		var enable = {"editable": false};
	// 		var page = this.getView().byId("detailPage");
	// 		if(route && route.toUpperCase().indexOf("edit".toUpperCase())>=0)
	// 		{
	// 			var clone = _.cloneDeep(data.getModel().getData());
	// 			var clonedModel = new sap.ui.model.json.JSONModel(clone);
	// 			this.getView().setModel(clonedModel, "c");
	// 			enable.editable = true;
	// 			var toolbar = sap.ui.xmlfragment("view.fragment.editToolbar", this);
	// 			page.setFooter(toolbar);
	// 			//Maybe to parameterize
	// 			this.populateSelect();
	// 			//------------------------
	// 		}
	// 		else
	// 		{
	// 			this.getView().setModel(data.getModel(), "c");
	// 			enable.editable=false;
	// 			var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar", this);
	// 			page.setFooter(toolbar);
	// 		}
	// 		var enableModel = new sap.ui.model.json.JSONModel(enable);
	// 		this.getView().setModel(enableModel, "en");
	//
	// },
	refreshView : function(data) //Could it be abstracted?
	{
			var enable = {"editable": false};
			var page = this.getView().byId("editPage");

			var clone = _.cloneDeep(data.getModel().getData());
			var clonedModel = new sap.ui.model.json.JSONModel(clone);
			this.getView().setModel(clonedModel, "c");
			enable.editable = true;
			var toolbar = sap.ui.xmlfragment("view.fragment.editToolbar", this);
			page.setFooter(toolbar);
			//Maybe to parameterize
			this.populateSelect();
				//-----------------------
			var enableModel = new sap.ui.model.json.JSONModel(enable);
			this.getView().setModel(enableModel, "en");

	},

	onSavePress :function()
	{
		var editedCustomer = this.getView().getModel("c").getData();
		this.customer.update(editedCustomer);
		this.router.navTo("customerDetail", {id:this.customer.getId()});
		// this.refreshView(this.customer);
	},
	onResetPress : function()
	{
		this.getView().getModel("c").setData(_.cloneDeep(this.customer.getModel().getData()));
	},

	populateSelect :  function()
	{
		//HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
		//HP2 : Every collections contains an array of params where a select is needed
		var pToSelArr = [
			{"type":"registryTypes", "namespace":"rt"},
			{"type":"billTypes", "namespace":"bt"},
			{"type":"contactTypes", "namespace":"ct"},
			{"type":"paymentConditions", "namespace": "pc"},
			{"type": "places", "namespace":"p"}
			];

		_.map(pToSelArr, _.bind(function(item)
	{
		utils.Collections.getModel(item.type)
		.then(_.bind
			(function(result)
		{
			this.getView().setModel(result, item.namespace);

		}, this))
	}, this));


	},
  
  openCustomerDetail: function(id)
  {
  model.collections.Customers.getById(id)
		.then(

			_.bind(function(result)
			{
				this.customer = result;
				// this.refreshView(result, routeName)
				this.refreshView(result);
			}, this)
		);
  },
  
 


});
