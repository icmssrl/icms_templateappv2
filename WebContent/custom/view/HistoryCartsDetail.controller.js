jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.HistoryCarts");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Discount");

view.abstract.AbstractController.extend("view.HistoryCartsDetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "historyCartsDetail"){
			return;
		}

		this.orderID = evt.getParameters().arguments.id;
        
        this.cart = model.collections.HistoryCarts.getById(this.orderID);
        
        var setFavoritebutton = this.getView().byId("markFavoriteButton");
            if(this.cart.favorite===false){
                setFavoritebutton.setText(model.i18n._getLocaleText("MARK_AS_FAVORITE"));
            }else{
                setFavoritebutton.setText(model.i18n._getLocaleText("REMOVE_FROM_FAVORITES"));
            }
        

            
        this.cartModel = new sap.ui.model.json.JSONModel(this.cart);
        //this.cartModel = historyCart.getModel();
        this.getView().setModel(this.cartModel, "c");
	
        this.customer = model.Current.getCustomer();


	},
	
//	refreshView : function(data)//maybe to abstract
//	{
//			var enable = {"editable": false};
//			var page = this.getView().byId("detailPage");
//			this.getView().setModel(data.getModel(), "c");
//			enable.editable=false;
////			var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar", this);
////			page.setFooter(toolbar);
//			var enableModel = new sap.ui.model.json.JSONModel(enable);
//			this.getView().setModel(enableModel, "en");
//
//	},
    
    onMarkAsFavoritePress: function(evt){
        var src = evt.getSource();
        var text = src.getProperty("text");
        var localObj = model.persistence.Storage.local.get("cartsHistory");
        var localArr = localObj.carts;
        
        var newLocalHistoryCarts;
        var cartListGlobalModel = sap.ui.getCore().getModel("globalCartList");
        var selectedCart = cartListGlobalModel.getData().historyCarts;
        var findId;
        var id = parseInt(this.orderID);
        selectedCart.map(function(obj){     
                            if (obj.orderId === id) findId = obj.orderId;    
                        });
        
        var test = cartListGlobalModel.getProperty("/historyCarts/"+(findId-1).toString()+"/favorite");
        
        if(text === model.i18n._getLocaleText("MARK_AS_FAVORITE"))
        {
               
            
            cartListGlobalModel.setProperty("/historyCarts/"+(findId-1).toString()+"/favorite", true);
            localArr.map(function(obj){     
                            if (obj.orderId === id){
                                obj.favorite = true; 
                            }    
                        });
            newLocalHistoryCarts = {"carts": localArr};
            model.persistence.Storage.local.remove("cartsHistory");
            model.persistence.Storage.local.save("cartsHistory", newLocalHistoryCarts);
            cartListGlobalModel.refresh(true);
            src.setText(model.i18n._getLocaleText("REMOVE_FROM_FAVORITES"));
            
        }else{
            
            cartListGlobalModel.setProperty("/historyCarts/"+(findId-1).toString()+"/favorite", false);
            localArr.map(function(obj){     
                            if (obj.orderId === id){
                                obj.favorite = false; 
                            }
                        });
            newLocalHistoryCarts = {"carts": localArr};
            model.persistence.Storage.local.remove("cartsHistory");
            model.persistence.Storage.local.save("cartsHistory", newLocalHistoryCarts);
            cartListGlobalModel.refresh(true);
            src.setText(model.i18n._getLocaleText("MARK_AS_FAVORITE"));
        }
        
    },
    
    activateHistoryCart: function(evt){
        if(this.cart.customerId || this.cart.customerName){
            var cloneOrder = jQuery.extend(true, {} , this.cart);
            cloneOrder.customerId = this.customer.registry.id;
            cloneOrder.customerName = this.customer.registry.customerName;
            if(!!cloneOrder.fullEvasionAvailableDate){
                delete cloneOrder.fullEvasionAvailableDate;
            }
            var orderModel = new model.Order();
            var positionsArr = [];
            
            var product = new model.Product();
            orderModel.update(cloneOrder);
            if(cloneOrder.positions || cloneOrder.positions.length >= 0)
                {
                    delete orderModel.positions;
                    
                    for(var i = 0; i< cloneOrder.positions.length; i++){
                        var position = new model.Position();
                        position.update(cloneOrder.positions[i]);
                        if(position.product){
                            
                            delete position.product;
                            product.update(cloneOrder.positions[i].product);
                            position.setProduct(product);
                        }
                        
                        var discount = new model.Discount();
                        discount.initialize(orderModel.getId(), position);
                        position.setDiscount(discount);
                        positionsArr.push(position);

                        
                    }
                    orderModel.setPositions(positionsArr);
                    model.Current.setOrder(orderModel);
                    model.persistence.Storage.session.save("currentOrder", orderModel);
                }
            
           
        }
        this.router.navTo("newOrder");
    },
    


	onEditPress : function()
	{
		this.router.navTo("customerEdit", {id:this.customer.getId()});
		// this.refreshView(this.customer, "edit");
	},

	onCreatePress:function(evt)
	{
		this.router.navTo("newCustomer");
	},
	

});
