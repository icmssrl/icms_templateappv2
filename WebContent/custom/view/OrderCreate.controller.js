jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Orders");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("view.abstract.AbstractMasterController");

view.abstract.AbstractMasterController.extend("view.OrderCreate", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    //this.notesModel = new sap.ui.model.json.JSONModel();
    //this.notesModel.setData({"billsItems":[], "salesItems":[]});
    //this.getView().setModel(this.notesModel,"notes");

    var oModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(oModel);


  },


  handleRouteMatched: function (evt) {

    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    var name = evt.getParameter("name");


    if (name !== "newOrder") {
      return;
    }

    //    this.getView().getModel("notes").refresh();

    this.customer = model.Current.getCustomer();
    if (!this.customer) {
      var customerData = model.persistence.Storage.session.get("currentCustomer");
      this.customer = new model.Customer(customerData);
    }
    this.getView().setModel(this.customer.getModel(), "customer");

    this.order = model.Current.getOrder();
      if(this.order){
          var destinations = {
            "items": []
          };
          destinations.items = this.customer.destinations;
          this.destinationsModel = new sap.ui.model.json.JSONModel(destinations);
          this.getView().setModel(this.destinationsModel, "d");
          this.refreshView(this.order);
      }
    

    if (!this.order) {
      this.order = new model.Order();
      this.order.create(this.customer.registry.id)
        .then(_.bind(function () {
          this.orderModel = this.order.getModel();
          this.getView().setModel(this.orderModel, "o");
          destinations = {
            "items": []
          };
          destinations.items = this.customer.destinations;
          this.destinationsModel = new sap.ui.model.json.JSONModel(destinations);
          this.getView().setModel(this.destinationsModel, "d");
        }, this));

    }






    this.populateSelect();
    // this.getView().byId("regAlrtBtn").setVisible(false);
    // this.getView().byId("ctAlrtBtn").setVisible(false);
    // this.getView().byId("bkAlrtBtn").setVisible(false);
    // this.getView().byId("slsAlrtBtn").setVisible(false);


  },
  refreshView: function (data) {
    this.getView().setModel(data.getModel(), "o");
    // var masterCntrl = this.getView().getModel("appStatus").getProperty("/masterCntrl");
    // masterCntrl.refreshList();
  },

  //Feed
  onPost: function (oEvent) {

    var id = oEvent.getSource().getId();
    var noteType = id.substr(id.lastIndexOf("-") + 1, id.length);


    // create new entry
    var sValue = oEvent.getParameter("value");
    var oEntry = {
      author: model.persistence.Storage.session.get("user").fullName,
      text: sValue
    };

    this.order.addNote(noteType, oEntry);

    // update model
    var oModel = this.order.getModel();
    this.getView().getModel("o").refresh();

  },



  onSavePress: function () {

    var saveButton = this.getView().byId("saveButton");
    saveButton.setEnabled(false);

    model.Current.setOrder(this.order);
    model.persistence.Storage.session.save("currentOrder", this.order);

//    var username = 'ADMINISTRATION01';
//    var password = 'Welcome1';
//
//    // var clientID = sap.ui.getCore().byId(this.getView().getId()).byId("code");
//    // var productID = sap.ui.getCore().byId(this.getView().getId()).byId("productID");
//    // var quantity = sap.ui.getCore().byId(this.getView().getId()).byId("quantity");
//    // var description = sap.ui.getCore().byId(this.getView().getId()).byId("description");
//    var counter = 1000;
//    var xmlhttp = new XMLHttpRequest();
//    xmlhttp.open('POST', 'https://my303832.crm.ondemand.com/sap/bc/srt/scs/sap/customerorderprocessingmanagec?sap-vhost=my303832.crm.ondemand.com/', true);
//    var sr =
//      '<?xml version="1.0" encoding="utf-8"?>' +
//      '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
//      'xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">' +
//      '<soapenv:Header/>' +
//      '<soapenv:Body>' +
//      '<glob:CustomerOrderBundleMaintainRequest_sync_V1>' +
//      //  '<BasicMessageHeader>'+
//      //
//      //     '<ID schemeAgencySchemeAgencyID="?">'+clientID.getValue()+'</ID>'+
//      //
//      //  '</BasicMessageHeader>'+
//      '<CustomerOrder actionCode="01" >' +
//      '<ProcessingTypeCode>OR</ProcessingTypeCode>' +
//      '<BuyerParty >' +
//      '<BusinessPartnerInternalID>' + this.customer.getId() + '</BusinessPartnerInternalID>' +
//      '</BuyerParty>' +
//      '<PayerParty>' +
//      '<BusinessPartnerInternalID>' + this.customer.getId() + '</BusinessPartnerInternalID>' +
//      '</PayerParty>' +
//      '<EmployeeResponsibleParty>' +
//      '<BusinessPartnerInternalID></BusinessPartnerInternalID>' +
//      '<EmployeeID></EmployeeID>' +
//      '</EmployeeResponsibleParty>';
//
//    if (this.order.getPositions() && this.order.getPositions().length > 0) {
//      for (var i = 0; i < this.order.getPositions().length; i++, counter++) {
//        sr += '<Item actionCode="01">' +
//          '<ID></ID>' +
//          '<ItemProduct>' +
//          '<ProductID>' + this.order.getPositions()[i].productId + '</ProductID>' +
//          '</ItemProduct>' +
//          '<ItemRequestedScheduleLine>' +
//          '<Quantity unitCode="EA">' + this.order.getPositions()[i].quantity + '</Quantity>' +
//          '</ItemRequestedScheduleLine>' +
//          '<ItemText actionCode="01">' +
//          '<TextTypeCode>10024</TextTypeCode>' +
//          '<ContentText>' + this.order.getPositions()[i].description + '</ContentText>' +
//          '</ItemText>' +
//          '</Item>'
//      }
//
//    }
//
//    sr += '</CustomerOrder>' +
//      '</glob:CustomerOrderBundleMaintainRequest_sync_V1>' +
//      '</soapenv:Body>' +
//      '</soapenv:Envelope>';
//
//
//
//    xmlhttp.onreadystatechange = function (r) {
//      if (xmlhttp.readyState == 4) {
//        if (xmlhttp.status == 200) {
//          saveButton.setEnabled(true);
//          console.log(r.target.responseText);
//          console.log(r.target.statusText);
//          sap.m.MessageToast.show("Inserimento Ordine OK!!", {
//            duration: 3000
//          });
//
//        } else {
//          saveButton.setEnabled(true);
//          sap.m.MessageToast.show("Error!! Please retry Save!", {
//            duration: 3000
//          });
//        }
//      }
//    };
//
//    xmlhttp.setRequestHeader("Authorization", "Basic " + btoa(username + ':' + password));
//    // Send the POST request
//    xmlhttp.setRequestHeader('Content-Type', 'text/xml');
//    xmlhttp.send(sr);


    // if(!this.validateCheck())
    // {
    // 		var failedControls = this.getFailedControls();
    // 		_.forEach(failedControls, function(item)
    // 	{
    // 		var jqueryCntrl = $('#'+item.getId());
    // 		var parents = jqueryCntrl.parents();
    // 		var jqueryPanel = _.find(parents, function(obj){ return (obj.className === "sapMPanel");});
    // 		var ui5Panel = sap.ui.getCore().byId(jqueryPanel.id);
    // 		var hBar = ui5Panel.getHeaderToolbar();
    // 		var icon = _.find(hBar.getContent(), _.bind(function(value){
    // 			var props = value.mProperties;
    // 			return props.hasOwnProperty("icon");
    // 		}, this));
    // 		icon.setVisible(true);
    // 	})
    //
    // 		sap.m.MessageToast.show("Client creation failed");
    // 		return;
    // }
    //
    // // adding directly costumer to collections costumers -> to substitute with create Entity and refresh collections
    // model.collections.Customers.addCustomers(this.customer);
    // sap.m.MessageToast.show("Client created with success");
    // this.router.navTo("launchpad");
  },
  onResetPress: function () {
    this.order = new model.Order();
    this.getView().getModel("o").refresh();
  },

  populateSelect: function () {
    //HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
    //HP2 : Every collections contains an array of params where a select is needed
    var pToSelArr = [
      {
        "type": "basketTypes",
        "namespace": "bt"
      },
      {
        "type": "billTypes",
        "namespace": "pm"
      },
      {
        "type": "resaTypes",
        "namespace": "r"
      },
      {
        "type": "meansShippingTypes",
        "namespace": "st"
      },
      {
        "type": "appointmentToDeliveryTypes",
        "namespace": "atd"
      },
      {
        "type": "deliveryTypes",
        "namespace": "dt"
      },
      {
        "type": "chargeTransportTypes",
        "namespace": "ctt"
      },
      {
        "type": "ivaTypes",
        "namespace": "it"
      },
			];

    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.getModel(item.type) //It could be abstracted internally with a method that get the respective odata from the type received
        .then(_.bind(function (result) {
          this.getView().setModel(result, item.namespace);

        }, this))
    }, this));
  },


  onAddProductsPress: function (evt) {
    if (this.order) {
      model.Current.setOrder(this.order);
      model.persistence.Storage.session.save("currentOrder", this.order);
    }
    this.router.navTo("empty");
  },

  //funzioni per allegare file


  formatAttribute: function (sValue) {
    jQuery.sap.require("sap.ui.core.format.FileSizeFormat");
    if (jQuery.isNumeric(sValue)) {
      return sap.ui.core.format.FileSizeFormat.getInstance({
        binaryFilesize: false,
        maxFractionDigits: 1,
        maxIntegerDigits: 3
      }).format(sValue);
    } else {
      return sValue;
    }
  },

  onChange: function (oEvent) {
    var oUploadCollection = oEvent.getSource();
    // Header Token
    var oCustomerHeaderToken = new sap.m.UploadCollectionParameter({
      name: "x-csrf-token",
      value: "securityTokenFromModel"
    });
    oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
  },

  onFileDeleted: function (oEvent) {
    var oData = this.getView().byId("UploadCollection").getModel().getData();
    if(oData.items)
    {
      var aItems = jQuery.extend(true, {}, oData).items;
    }
    else
    {
      var aItems = [];
    }
    
    var sDocumentId = oEvent.getParameter("documentId");
    jQuery.each(aItems, function (index) {
      if (aItems[index] && aItems[index].documentId === sDocumentId) {
        aItems.splice(index, 1);
      };
    });
    this.getView().byId("UploadCollection").getModel().setData({
      "items": aItems
    });
    var oUploadCollection = oEvent.getSource();
    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
    sap.m.MessageToast.show("FileDeleted event triggered.");
  },

  onFileRenamed: function (oEvent) {
    var oData = this.getView().byId("UploadCollection").getModel().getData();
    if(oData.items)
    {
      var aItems = jQuery.extend(true, {}, oData).items;
    }
    else
    {
      var aItems = [];
    }
    var sDocumentId = oEvent.getParameter("documentId");
    jQuery.each(aItems, function (index) {
      if (aItems[index] && aItems[index].documentId === sDocumentId) {
        aItems[index].fileName = oEvent.getParameter("item").getFileName();
      };
    });
    this.getView().byId("UploadCollection").getModel().setData({
      "items": aItems
    });
    sap.m.MessageToast.show("FileRenamed event triggered.");
  },

  onFileSizeExceed: function (oEvent) {
    sap.m.MessageToast.show("FileSizeExceed event triggered.");
  },

  onTypeMissmatch: function (oEvent) {
    sap.m.MessageToast.show("TypeMissmatch event triggered.");
  },

  onUploadComplete: function (oEvent) {
    var oData = this.getView().byId("UploadCollection").getModel().getData();
    if(oData.items)
    {
      var aItems = jQuery.extend(true, {}, oData).items;
    }
    else
    {
      var aItems = [];
    }
    var oItem = {};
    var sUploadedFile = oEvent.getParameter("files")[0].fileName;
    // at the moment parameter fileName is not set in IE9
    if (!sUploadedFile) {
      var aUploadedFile = (oEvent.getParameters().getSource().getProperty("value")).split(/\" "/);
      sUploadedFile = aUploadedFile[0];
    }
    oItem = {
      "documentId": jQuery.now().toString(), // generate Id,
      "fileName": sUploadedFile,
      "mimeType": "",
      "thumbnailUrl": "",
      "url": "",
      "attributes": [
        {
          "title": "Uploaded By",
          "text": model.persistence.Storage.session.get("user").fullName,
						},
        {
          "title": "Uploaded On",
          "text": new Date(jQuery.now()).toLocaleDateString()
						},
        {
          "title": "File Size",
          "text": "505000"
						}
					]
    };
    aItems.unshift(oItem);
    this.getView().byId("UploadCollection").getModel().setData({
      "items": aItems
    });
    var oUploadCollection = oEvent.getSource();
    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
    // delay the success message for to notice onChange message
    setTimeout(function () {
      sap.m.MessageToast.show("UploadComplete event triggered.");
    }, 4000);
  },

  onSelectChange: function (oEvent) {
    var oUploadCollection = sap.ui.getCore().byId(this.getView().getId() + "--UploadCollection");
    oUploadCollection.setShowSeparators(oEvent.getParameters().selectedItem.getProperty("key"));
  },
  onBeforeUploadStarts: function (oEvent) {
    // Header Slug
    var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
      name: "slug",
      value: oEvent.getParameter("fileName")
    });
    oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
    sap.m.MessageToast.show("BeforeUploadStarts event triggered.");
  },
  onUploadTerminated: function (oEvent) {
    // get parameter file name
    var sFileName = oEvent.getParameter("fileName");
    // get a header parameter (in case no parameter specified, the callback function getHeaderParameter returns all request headers)
    var oRequestHeaders = oEvent.getParameters().getHeaderParameter();
  }




  // onStartSessionPress : function()
  // {
  // 	model.Persistence.session.save("customerSession", true);
  // 	model.Persistence.session.save("currentCustomer", this.customer);
  // 	this.router.navTo("launchpad");
  // }


});