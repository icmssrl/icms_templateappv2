jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Orders");
jQuery.sap.require("model.collections.TestOrders");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("view.abstract.AbstractMasterController");

view.abstract.AbstractMasterController.extend("view.OrderInfo", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    //this.notesModel = new sap.ui.model.json.JSONModel();
    //this.notesModel.setData({"billsItems":[], "salesItems":[]});
    //this.getView().setModel(this.notesModel,"notes");
      
    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");

    var oModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(oModel);
      
    this.testOrderModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.testOrderModel, "testOrder");
      
    this.testPositionModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.testPositionModel, "testPosition");

  },


  handleRouteMatched: function (evt) {

    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    var name = evt.getParameter("name");
      
    var id = evt.getParameters().arguments.id;


    if (name !== "orderInfo") {
      return;
    }
      
    this.user = model.persistence.Storage.session.get("user");
    this.userModel.setData(this.user);
      
       model.collections.TestOrders.loadTestOrdersById(id) //forceReload
      .then(_.bind(function(res){
            console.log(res);
           
            this.testOrderModel.setData(res);
            this.testOrderModel.refresh(true);
            this.testPositionModel.setData(res.positions);
            this.testPositionModel.refresh(true);
            //var oTable = this.getView().byId("orderListTable");

          }, this));

    //    this.getView().getModel("notes").refresh();

//    this.customer = model.Current.getCustomer();
//    if (!this.customer) {
//      var customerData = model.persistence.Storage.session.get("currentCustomer");
//      this.customer = new model.Customer(customerData);
//    }
//    this.getView().setModel(this.customer.getModel(), "customer");
//
//    this.order = model.Current.getOrder();
//
//    if (!this.order) {
//      this.order = new model.Order();
//      this.order.create(this.customer.registry.id)
//        .then(_.bind(function () {
//          this.orderModel = this.order.getModel();
//          this.getView().setModel(this.orderModel, "o");
//          var destinations = {
//            "items": []
//          };
//          destinations.items = this.customer.destinations;
//          this.destinationsModel = new sap.ui.model.json.JSONModel(destinations);
//          this.getView().setModel(this.destinationsModel, "d");
//        }, this));
//
//    }
//
//
//
//
//
//
//    this.populateSelect();
//    // this.getView().byId("regAlrtBtn").setVisible(false);
//    // this.getView().byId("ctAlrtBtn").setVisible(false);
//    // this.getView().byId("bkAlrtBtn").setVisible(false);
//    // this.getView().byId("slsAlrtBtn").setVisible(false);


  },
    
  toOrderListPress:function(evt){
        this.router.navTo("orderList");
  }

    
    
//    
//  refreshView: function (data) {
//    this.getView().setModel(data.getModel(), "o");
//    // var masterCntrl = this.getView().getModel("appStatus").getProperty("/masterCntrl");
//    // masterCntrl.refreshList();
//  },
//
//  //Feed
//  onPost: function (oEvent) {
//
//    var id = oEvent.getSource().getId();
//    var noteType = id.substr(id.lastIndexOf("-") + 1, id.length);
//
//
//    // create new entry
//    var sValue = oEvent.getParameter("value");
//    var oEntry = {
//      author: model.persistence.Storage.session.get("user").fullName,
//      text: sValue
//    };
//
//    this.order.addNote(noteType, oEntry);
//
//    // update model
//    var oModel = this.order.getModel();
//    this.getView().getModel("o").refresh();
//
//  },
//
//
//
//  onSavePress: function () {
//
//    var saveButton = this.getView().byId("saveButton");
//    saveButton.setEnabled(false);
//
//    model.Current.setOrder(this.order);
//    model.persistence.Storage.session.save("currentOrder", this.order);
//
//    
//  },
//  onResetPress: function () {
//    this.order = new model.Order();
//    this.getView().getModel("o").refresh();
//  },
//

//
//  onAddProductsPress: function (evt) {
//    if (this.order) {
//      model.Current.setOrder(this.order);
//      model.persistence.Storage.session.save("currentOrder", this.order);
//    }
//    this.router.navTo("empty");
//  },
//
//  //funzioni per allegare file
//
//
//  formatAttribute: function (sValue) {
//    jQuery.sap.require("sap.ui.core.format.FileSizeFormat");
//    if (jQuery.isNumeric(sValue)) {
//      return sap.ui.core.format.FileSizeFormat.getInstance({
//        binaryFilesize: false,
//        maxFractionDigits: 1,
//        maxIntegerDigits: 3
//      }).format(sValue);
//    } else {
//      return sValue;
//    }
//  },
//
//  onChange: function (oEvent) {
//    var oUploadCollection = oEvent.getSource();
//    // Header Token
//    var oCustomerHeaderToken = new sap.m.UploadCollectionParameter({
//      name: "x-csrf-token",
//      value: "securityTokenFromModel"
//    });
//    oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
//  },
//
//  onFileDeleted: function (oEvent) {
//    var oData = this.getView().byId("UploadCollection").getModel().getData();
//    if(oData.items)
//    {
//      var aItems = jQuery.extend(true, {}, oData).items;
//    }
//    else
//    {
//      var aItems = [];
//    }
//    
//    var sDocumentId = oEvent.getParameter("documentId");
//    jQuery.each(aItems, function (index) {
//      if (aItems[index] && aItems[index].documentId === sDocumentId) {
//        aItems.splice(index, 1);
//      };
//    });
//    this.getView().byId("UploadCollection").getModel().setData({
//      "items": aItems
//    });
//    var oUploadCollection = oEvent.getSource();
//    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
//    sap.m.MessageToast.show("FileDeleted event triggered.");
//  },
//
//  onFileRenamed: function (oEvent) {
//    var oData = this.getView().byId("UploadCollection").getModel().getData();
//    if(oData.items)
//    {
//      var aItems = jQuery.extend(true, {}, oData).items;
//    }
//    else
//    {
//      var aItems = [];
//    }
//    var sDocumentId = oEvent.getParameter("documentId");
//    jQuery.each(aItems, function (index) {
//      if (aItems[index] && aItems[index].documentId === sDocumentId) {
//        aItems[index].fileName = oEvent.getParameter("item").getFileName();
//      };
//    });
//    this.getView().byId("UploadCollection").getModel().setData({
//      "items": aItems
//    });
//    sap.m.MessageToast.show("FileRenamed event triggered.");
//  },
//
//  onFileSizeExceed: function (oEvent) {
//    sap.m.MessageToast.show("FileSizeExceed event triggered.");
//  },
//
//  onTypeMissmatch: function (oEvent) {
//    sap.m.MessageToast.show("TypeMissmatch event triggered.");
//  },
//
//  onUploadComplete: function (oEvent) {
//    var oData = this.getView().byId("UploadCollection").getModel().getData();
//    if(oData.items)
//    {
//      var aItems = jQuery.extend(true, {}, oData).items;
//    }
//    else
//    {
//      var aItems = [];
//    }
//    var oItem = {};
//    var sUploadedFile = oEvent.getParameter("files")[0].fileName;
//    // at the moment parameter fileName is not set in IE9
//    if (!sUploadedFile) {
//      var aUploadedFile = (oEvent.getParameters().getSource().getProperty("value")).split(/\" "/);
//      sUploadedFile = aUploadedFile[0];
//    }
//    oItem = {
//      "documentId": jQuery.now().toString(), // generate Id,
//      "fileName": sUploadedFile,
//      "mimeType": "",
//      "thumbnailUrl": "",
//      "url": "",
//      "attributes": [
//        {
//          "title": "Uploaded By",
//          "text": model.persistence.Storage.session.get("user").fullName,
//						},
//        {
//          "title": "Uploaded On",
//          "text": new Date(jQuery.now()).toLocaleDateString()
//						},
//        {
//          "title": "File Size",
//          "text": "505000"
//						}
//					]
//    };
//    aItems.unshift(oItem);
//    this.getView().byId("UploadCollection").getModel().setData({
//      "items": aItems
//    });
//    var oUploadCollection = oEvent.getSource();
//    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
//    // delay the success message for to notice onChange message
//    setTimeout(function () {
//      sap.m.MessageToast.show("UploadComplete event triggered.");
//    }, 4000);
//  },
//
//  onSelectChange: function (oEvent) {
//    var oUploadCollection = sap.ui.getCore().byId(this.getView().getId() + "--UploadCollection");
//    oUploadCollection.setShowSeparators(oEvent.getParameters().selectedItem.getProperty("key"));
//  },
//  onBeforeUploadStarts: function (oEvent) {
//    // Header Slug
//    var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
//      name: "slug",
//      value: oEvent.getParameter("fileName")
//    });
//    oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
//    sap.m.MessageToast.show("BeforeUploadStarts event triggered.");
//  },
//  onUploadTerminated: function (oEvent) {
//    // get parameter file name
//    var sFileName = oEvent.getParameter("fileName");
//    // get a header parameter (in case no parameter specified, the callback function getHeaderParameter returns all request headers)
//    var oRequestHeaders = oEvent.getParameters().getHeaderParameter();
//  }
//




});