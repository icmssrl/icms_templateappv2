jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.collections.Positions");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.Order");
jQuery.sap.require("model.Current");
jQuery.sap.require("model.Position");
jQuery.sap.require("model.Cart");

view.abstract.AbstractController.extend("view.CartFull", {

  onExit: function () {

  },


  onInit: function () {

    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    this.valueStateModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.valueStateModel, "vs");
    this.valueStateModel.setProperty("valueState", "");

  },


  handleRouteMatched: function (evt) {

    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    var routeName = evt.getParameter("name");

    if (routeName !== "cartFullView") {
      return;
    }

    this.order = model.Current.getOrder();
    if (!this.order) {
      var orderData = model.persistence.Storage.session.get("currentOrder");
      this.order = new model.Order(orderData);
    };
    this.customer = model.Current.getCustomer();
    if (!this.customer) {
      var customerData = model.persistence.Storage.session.get("currentCustomer");
      this.customer = new model.Customer(customerData);
    }
    this.getView().setModel(this.customer.getModel(), "customer");


    this.cartModel = this.order.getModel();
    this.getView().setModel(this.cartModel, "c");


    this.calculateFunction();
  },

  onSave: function () {

        console.log(this.order);
        var positions = this.order.positions;
        for(var pos = 0; pos<positions.length; pos++){
            if(positions[pos].quantity===0 || positions[pos].quantity===undefined){
                sap.m.MessageBox.alert(model.i18n._getLocaleText("0_QUANTITY"), {
                    title: model.i18n._getLocaleText("WARNING")                                      
                });
                return;
            }
        }
        var that = this;
        var arr = [];
        
      
      ///////****************************************************/////////////
                    
                    sap.m.MessageBox.show(
                        model.i18n._getLocaleText("CONFIRM_CART_SAVE_TEXT"), {
                          icon: sap.m.MessageBox.Icon.QUESTION,
                          title: model.i18n._getLocaleText("CONFIRM_CART_SAVE_TITLE"),
                          actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                          onClose: function(oAction) 
                            { / * test save * / 
                                console.log(oAction);
                                if(oAction==="YES"){
                                    
                                    //TODO invio dati del ordine a SAP
                                    
                                    
                                    sap.m.MessageToast.show(model.i18n._getLocaleText("ORDER_SAVED"));
                                    
                                    
                                    ///////////********************************//////////////////////
                                    
                                    sap.m.MessageBox.show(
                                        model.i18n._getLocaleText("ASK_TO_STORIFY_CART"), {
                                          icon: sap.m.MessageBox.Icon.QUESTION,
                                          title: model.i18n._getLocaleText("STORIFY_CART_TITLE"),
                                          actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                                          onClose: function(oAction) 
                                            { / * test save * / 
                                                console.log(oAction);
                                                if(oAction==="YES"){
                                                    that.order.flagHistoryCart = true;
                                                    if(!!model.persistence.Storage.local.get("cartsHistory")){
                                                        var local = model.persistence.Storage.local.get("cartsHistory");
                                                        var maxid = 0;
                                                        local.carts.map(function(obj){     
                                                            if (obj.orderId > maxid) maxid = obj.orderId;    
                                                        });

                                                        var cnt = maxid+1;
                                                        that.order.orderId = cnt;
                                                        local.carts.push(that.order);

                                                        model.persistence.Storage.local.save("cartsHistory", {"carts": local.carts});

                                                    }else{
                                                    that.order.orderId = 1;
                                                    arr.push(that.order);
                                                    model.persistence.Storage.local.save("cartsHistory", {"carts": arr});
                                                    }
                                                    sap.m.MessageToast.show(model.i18n._getLocaleText("CART_LOCALLY_SAVED"));
                                                    
                                                    if(!!model.persistence.Storage.session.get("currentOrder")){
                                                        model.Current.removeOrder();
                                                    }
                                                    setTimeout(that.router.navTo("newOrder"), 4000);
                                                    



                                                }else{
                                                    if(!!model.persistence.Storage.session.get("currentOrder")){
                                                        model.Current.removeOrder();
                                                    }
                                                    setTimeout(that.router.navTo("newOrder"), 4000);
                                                    //that.router.navTo("newOrder");
                                                }
                                            }
                                      }
                                    );
                                    
                                    
                                    
                                    
                                    ///////////*******************************/////////////////////
                                    
                                    
                                    
                                }else{
                                    return;
                                }
                            }
                      }
                    );
                    
                    
                    //////******************************************************//////////
      
    // var username = 'ADMINISTRATION01';
    // var password = 'Welcome1';
    //
    // // var clientID = sap.ui.getCore().byId(this.getView().getId()).byId("code");
    // // var productID = sap.ui.getCore().byId(this.getView().getId()).byId("productID");
    // // var quantity = sap.ui.getCore().byId(this.getView().getId()).byId("quantity");
    // // var description = sap.ui.getCore().byId(this.getView().getId()).byId("description");
    // var counter = 1000;
    // var xmlhttp = new XMLHttpRequest();
    // xmlhttp.open('POST', 'https://my303832.crm.ondemand.com/sap/bc/srt/scs/sap/customerorderprocessingmanagec?sap-vhost=my303832.crm.ondemand.com/', true);
    //
    //
    // var sr =
    //   '<?xml version="1.0" encoding="utf-8"?>' +
    //
    //   '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
    //   'xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">' +
    //   '<soapenv:Header/>' +
    //   '<soapenv:Body>' +
    //   '<glob:CustomerOrderBundleMaintainRequest_sync_V1>' +
    //
    //   //  '<BasicMessageHeader>'+
    //   //
    //   //     '<ID schemeAgencySchemeAgencyID="?">'+clientID.getValue()+'</ID>'+
    //   //
    //   //  '</BasicMessageHeader>'+
    //   '<CustomerOrder actionCode="01" >' +
    //   '<ProcessingTypeCode>OR</ProcessingTypeCode>' +
    //   '<BuyerParty >' +
    //   '<BusinessPartnerInternalID>' + this.customer.getId() + '</BusinessPartnerInternalID>' +
    //   '</BuyerParty>' +
    //
    //   '<PayerParty>' +
    //   '<BusinessPartnerInternalID>' + this.customer.getId() + '</BusinessPartnerInternalID>' +
    //   '</PayerParty>' +
    //   '<EmployeeResponsibleParty>' +
    //   '<BusinessPartnerInternalID></BusinessPartnerInternalID>' +
    //   '<EmployeeID></EmployeeID>' +
    //   '</EmployeeResponsibleParty>';
    //
    // if (this.order.getPositions() && this.order.getPositions().length > 0) {
    //   for (var i = 0; i < this.order.getPositions().length; i++, counter++) {
    //     sr += '<Item actionCode="01">' +
    //       '<ID></ID>' +
    //
    //       '<Description languageCode="">' + this.order.getPositions()[i].description + '</Description>' +
    //
    //       '<ItemProduct>' +
    //       '<ProductID>' + this.order.getPositions()[i].productId + '</ProductID>' +
    //       '</ItemProduct>' +
    //       '<ItemRequestedScheduleLine>' +
    //       '<Quantity unitCode="EA">' + this.order.getPositions()[i].quantity + '</Quantity>' +
    //       '</ItemRequestedScheduleLine>' +
    //       '<ItemText actionCode="01">' +
    //
    //
    //       '<TextTypeCode>10011</TextTypeCode>' +
    //       '<ContentText>"test"</ContentText>' +
    //
    //       '</ItemText>' +
    //       '</Item>'
    //   }
    //
    // }
    //
    // sr += '</CustomerOrder>' +
    //   '</glob:CustomerOrderBundleMaintainRequest_sync_V1>' +
    //   '</soapenv:Body>' +
    //   '</soapenv:Envelope>';
    //
    //
    //
    // xmlhttp.onreadystatechange = function (r) {
    //   if (xmlhttp.readyState == 4) {
    //     if (xmlhttp.status == 200) {
    //
    //       console.log(r.target.responseText);
    //       console.log(r.target.statusText);
    //       sap.m.MessageToast.show("Inserimento Ordine OK!!", {
    //         duration: 3000
    //
    //       });
    //
    //     } else {
    //       sap.m.MessageToast.show("Error!! Please retry Save!", {
    //         duration: 3000
    //
    //       });
    //     }
    //   }
    // }
    //
    // xmlhttp.setRequestHeader("Authorization", "Basic " + btoa(username + ':' + password));
    // // Send the POST request
    // xmlhttp.setRequestHeader('Content-Type', 'text/xml');
    // xmlhttp.send(sr);
    // send request
    // ...
  },
  onShowProductPress: function (evt) {
//    var product = this.getView().getModel("appStatus").getProperty("/currentProduct");
//    if (product)
//      this.router.navTo("productDetail", {
//        id: product.getId()
//      });
//    else {
//      this.router.navTo("empty");
//    }
      this.router.navTo("empty");
      
  },
  onOrderHeaderPress: function (evt) {
    this.router.navTo("newOrder");
  },

  /********
  Primo metodo delete Rows che elimina dinamicamente dal model bindato alla view
  la posizione dall'array positions
  *************************************/ ///////
  onCancelRowPress: function (evt) {
    var position = evt.getParameters().listItem;
    var oContext = position.getBindingContext("c");
    var oModel = oContext.getModel();
    var row = oContext.getObject();
    var positionId = row.getId();
    this.order.removePosition(positionId);


    //        var oData = oModel.getData();
    //        var currentRowIndex = parseInt(oContext.getPath().split('/')[oContext.getPath().split('/').length - 1]);
    //        oData.positions.splice(currentRowIndex,1);
    oModel.updateBindings();
    this.getView().getModel("c").refresh(true);


  },

  onChangeQuantity: function (evt) {
    //var compareDate=this.order;
    //var quantityState = sap.ui.getCore().byId(this.getView().getId()).byId("inputQuantity");
    var inputID = evt.getParameters().id;
    var inputField = sap.ui.getCore().byId(inputID);
    var compareDate = new Date();
    var obj = evt.getSource().getBindingContext("c").getObject();
    obj.calculateTotalListPrice();
    var requestedDate = new Date(this.order.requestedDate); //.toLocaleDateString();
    //var cModel = this.getView().getModel("c");

    model.collections.Positions.getFutureDateByProductId(obj.productId, obj.quantity, requestedDate) //forceReload
      .then(_.bind(function (res) {
        console.log(res);
        var temp = {};
        temp.availableDate = res.availableDate.toLocaleDateString();
        temp.available = res.success;
        obj.update(temp);
        if (res.success === true) {
          inputField.setValueState("Success");
        } else {
          inputField.setValueState("None");
        }
        if (!this.order.fullEvasionAvailableDate || new Date(this.order.fullEvasionAvailableDate) < new Date(res.availableDate)) {
          this.order.fullEvasionAvailableDate = new Date(res.availableDate).toLocaleDateString();
        }
        this.cartModel.refresh(true);
        temp = {};
        //cModel.setProperty("/available", res.success);
        //cModel.setProperty("/availableDate", res.availableDate.toLocaleDateString());
        //cModel.updateBindings();
      }, this));

    //        var position = model.getData().positions;
    //
    //        for(var i=0; i<positions.length; i++){
    //            position[i].calculateTotalListPrice();
    //            //positions[i].totalListPrice = parseInt(positions[i].quantity) * parseFloat(positions[i].unitListPrice);
    //        }
    //model.updateBindings();
  },

  onSetDiscountPress: function (evt) {
    var src = evt.getSource();
    var position = src.getBindingContext("c").getObject();
    var discount = position.getDiscount();
    //var discount = position.discount;
    var discountModel = discount.getModel();
    //discountModel.setData(discount);
//    for (var prop in discount) {
//        if (_.isObject(this[prop])) {
//          _.merge(discountModel.getData()[prop], discount[prop]);
//        } else {
//          discountModel.getData()[prop] = discount[prop];
//        }
//      }
    
    
    var page = this.getView().byId("cartPage");
    this.discountDialog = sap.ui.xmlfragment("view.dialog.discountDialog", this);
    this.discountDialog.setModel(discountModel, "d");
    page.addDependent(this.discountDialog);

    this.discountDialog.open();
  },
  onDiscountDialogClose: function (evt) {
    this.discountDialog.close();

  },
  onDiscountDialogOK: function (evt) {

    console.log(this.order);
    this.discountDialog.close();

  },
  refreshDiscount: function (evt) {
    var discount = this.discountDialog.getModel("d").getData().ref;
    this.discountDialog.setModel(discount.refreshModel(), "d");
  },


  // refreshView : function(data, route)
  // {
  // 		var enable = {"editable": false};
  // 		var page = this.getView().byId("detailPage");
  // 		if(route && route.toUpperCase().indexOf("edit".toUpperCase())>=0)
  // 		{
  // 			var clone = _.cloneDeep(data.getModel().getData());
  // 			var clonedModel = new sap.ui.model.json.JSONModel(clone);
  // 			this.getView().setModel(clonedModel, "c");
  // 			enable.editable = true;
  // 			var toolbar = sap.ui.xmlfragment("view.fragment.editToolbar", this);
  // 			page.setFooter(toolbar);
  // 			//Maybe to parameterize
  // 			this.populateSelect();
  // 			//------------------------
  // 		}
  // 		else
  // 		{
  // 			this.getView().setModel(data.getModel(), "c");
  // 			enable.editable=false;
  // 			var toolbar = sap.ui.xmlfragment("view.fragment.detailToolbar", this);
  // 			page.setFooter(toolbar);
  // 		}
  // 		var enableModel = new sap.ui.model.json.JSONModel(enable);
  // 		this.getView().setModel(enableModel, "en");
  //
  // },
  // refreshView : function(data)//maybe to abstract
  // {
  // 		var enable = {"editable": false};
  // 		var page = this.getView().byId("detailPage");
  // 		this.getView().setModel(data.getModel(), "p");
  // 		enable.editable=false;
  // 		var toolbar = sap.ui.xmlfragment("view.fragment.catalogueToolbar", this);
  // 		page.setFooter(toolbar);
  // 		// var enableModel = new sap.ui.model.json.JSONModel(enable);
  // 		// this.getView().setModel(enableModel, "en");
  //
  // },

  // onAddPress : function(evt)
  // {
  // 		this.order = model.Current.getOrder();
  //
  // 		//Now it's here, maybe the next code needs to be moved
  // 		//As we are in orderCreqtion, maybe we need another property to verify the application status
  // 			if(!this.order)
  // 			{
  // 				var orderData = model.persistence.Storage.session.get("currentOrder");
  // 				this.order  = new model.Order(orderData);
  //
  // 			}
  //
  // 		//------------------------------------------------
  // 		var position = new model.Position();
  // 		position.create(this.order, this.product);
  // 		this.order.addPosition(position);
  // 		model.Current.setOrder(this.order);
  // 		console.log(this.order);
  //
  //
  //
  // }
  // onStartSessionPress : function()
  // {
  // 	model.persistence.Storage.session.save("customerSession", true);
  // 	model.persistence.Storage.session.save("currentCustomer", this.customer);
  // 	//Maybe is better correcting save class
  // 	model.Current.setCustomer(this.customer);
  // 	this.router.navTo("launchpad");
  // },
  // onEditPress : function()
  // {
  // 	this.router.navTo("customerEdit", {id:this.customer.getId()});
  // 	// this.refreshView(this.customer, "edit");
  // },

  // onCreatePress:function(evt)
  // {
  // 	this.router.navTo("newCustomer");
  // }
  // onSavePress :function()
  // {
  // 	var editedCustomer = this.getView().getModel("c").getData();
  // 	this.customer.update(editedCustomer);
  // 	this.router.navTo("customerDetail", {id:this.customer.getId()});
  // 	// this.refreshView(this.customer);
  // },
  // onResetPress : function()
  // {
  // 	this.getView().getModel("c").setData(_.cloneDeep(this.customer.getModel().getData()));
  // },

  // populateSelect :  function()
  // {
  // 	//HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
  // 	//HP2 : Every collections contains an array of params where a select is needed
  // 	var pToSelArr = [
  // 		{"type":"registryTypes", "namespace":"rt"},
  // 		{"type":"billTypes", "namespace":"bt"},
  // 		{"type":"contactTypes", "namespace":"ct"},
  // 		{"type":"paymentConditions", "namespace": "pc"},
  // 		{"type": "places", "namespace":"p"}
  // 		];
  //
  // 	_.map(pToSelArr, _.bind(function(item)
  // {
  // 	utils.Collections.getModel(item.type)
  // 	.then(_.bind
  // 		(function(result)
  // 	{
  // 		this.getView().setModel(result, item.namespace);
  //
  // 	}, this))
  // }, this));
  //
  //
  // }

  availableDateFunction: function (obj) {

    var compareDate = new Date();

    var requestedDate = new Date(obj.wantedDate); //.toLocaleDateString();


    model.collections.Positions.getFutureDateByProductId(obj.productId, obj.quantity, requestedDate) //forceReload
      .then(_.bind(function (res) {
        console.log(res);
        var temp = {};
        temp.availableDate = res.availableDate.toLocaleDateString();
        temp.available = res.success;
        obj.update(temp);
        //            if(res.success===true){
        //            inputField.setValueState("Success");
        //            }else{
        //                inputField.setValueState("None");
        //            }
        //**
        var d = undefined;
        if (this.order.fullEvasionAvailableDate) {
          var f = this.order.fullEvasionAvailableDate;
          if(f.indexOf('/')>0){
          d = this.order.fullEvasionAvailableDate.split('/');
            d = d[0] + "-" + d[1] + "-" + d[2];
          }
          else if(f.indexOf('-')>0)
          {
           d = this.order.fullEvasionAvailableDate.split('-'); 
            d = d[0] + "-" + d[1] + "-" + d[2];
          }
          
          
        }
        //**
        if (d) {
          if ( /*!this.order.fullEvasionAvailableDate ||*/ new Date(d) < new Date(res.availableDate)) {
            var da = new Date(res.availableDate).toLocaleDateString();
            if(da.indexOf("/")>0)
            {
              da=da.split("/");
            }
            else if(da.indexOf("-"))
            {
              da=da.split("-");
            }
            this.order.fullEvasionAvailableDate = da[2] + "-" + da[1] + "-" + da[0];
          } else {
            var da = new Date(d).toLocaleDateString();
            if(da.indexOf("/")>0)
            {
              da=da.split("/");
            }
            else if(da.indexOf("-"))
            {
              da=da.split("-");
            }
            this.order.fullEvasionAvailableDate = da[2] + "-" + da[1] + "-" + da[0];
          }
        } else {
          this.order.fullEvasionAvailableDate = this.order.requestedDate;
        }
        //            var modelObj = obj.getModel();
        //            modelObj.updateBindings();
        this.cartModel.refresh(true);
        temp = {};

      }, this));
  },





  calculateFunction: function () {
    if (this.order && this.order.positions.length > 0) {
      for (var i = 0; i < this.order.positions.length; i++) {
        this.availableDateFunction(this.order.positions[i]);
      }
    }

  }

});