jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Customers");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Customer");
jQuery.sap.require("view.abstract.AbstractMasterController");



view.abstract.AbstractMasterController.extend("view.CustomerCreate", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var name = evt.getParameter("name");

		if ( name !== "newCustomer" ){
			return;
		}

		this.user = model.persistence.Storage.session.get("user");
		this.division = model.persistence.Storage.session.get("division");

		this.customer = new model.Customer();
		this.customer.initialize(this.user, this.division);
		
		this.customerModel = this.customer.getModel();
		this.getView().setModel(this.customerModel, "c");
		this.populateSelect();
		this.getView().byId("regAlrtBtn").setVisible(false);
		this.getView().byId("ctAlrtBtn").setVisible(false);
		this.getView().byId("bkAlrtBtn").setVisible(false);
		this.getView().byId("slsAlrtBtn").setVisible(false);


	},
	refreshView : function(data)
	{
			this.getView().setModel(data.getModel(), "c");
			// var masterCntrl = this.getView().getModel("appStatus").getProperty("/masterCntrl");
			// masterCntrl.refreshList();
	},
	onSavePress : function()
	{

		if(!this.validateCheck())
		{
				var failedControls = this.getFailedControls();
				_.forEach(failedControls, function(item)
			{
				var jqueryCntrl = $('#'+item.getId());
				var parents = jqueryCntrl.parents();
				var jqueryPanel = _.find(parents, function(obj){ return (obj.className === "sapMPanel");});
				var ui5Panel = sap.ui.getCore().byId(jqueryPanel.id);
				var hBar = ui5Panel.getHeaderToolbar();
				var icon = _.find(hBar.getContent(), _.bind(function(value){
					var props = value.mProperties;
					return props.hasOwnProperty("icon");
				}, this));
				icon.setVisible(true);
			})

				sap.m.MessageToast.show("Client creation failed");
				return;
		}

		// adding directly costumer to collections costumers -> to substitute with create Entity and refresh collections
		model.collections.Customers.addCustomers(this.customer);
		sap.m.MessageToast.show("Client created with success");
		this.router.navTo("launchpad");
	},
	onResetPress : function()
	{
		this.customer = new model.Customer();
		this.getView().getModel("c").refresh();
	},

	populateSelect :  function()
	{
		//HP1 : Maybe it's possible creating a mapping file on which every collection has its params to select
		//HP2 : Every collections contains an array of params where a select is needed
		var pToSelArr = [
			{"type":"registryTypes", "namespace":"rt"},
			{"type":"billTypes", "namespace":"bt"},
			{"type":"contactTypes", "namespace":"ct"},
			{"type":"paymentConditions", "namespace": "pc"},
			{"type": "places", "namespace":"p"}
			];

		_.map(pToSelArr, _.bind(function(item)
	{
		utils.Collections.getModel(item.type) //It could be abstracted internally with a method that get the respective odata from the type received
		.then(_.bind
			(function(result)
		{
			this.getView().setModel(result, item.namespace);

		}, this))
	}, this));
}

	// onStartSessionPress : function()
	// {
	// 	model.Persistence.session.save("customerSession", true);
	// 	model.Persistence.session.save("currentCustomer", this.customer);
	// 	this.router.navTo("launchpad");
	// }


});
