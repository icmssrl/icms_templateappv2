jQuery.sap.declare("utils.Formatter");

utils.Formatter = {

	formatName: function ( value ) {
		//var tempValue = value.toUpperCase();
		console.log(value);
		return value;
	},

	formatCustomer: function (value) {
		return (!value) ? "Warning" : "None";
	},

	formatCustomerText: function (value) {
		if( value ){
			return "";
		}
		else{
			return "is not a customer! call him!";
		}
		return "";
	},

	canCreate : function(value)
	{
		if(value.toUpperCase().indexOf("agent".toUpperCase())>=0)
			return true;
		else {
			return false;
		}
	},
	canEdit : function(value)
	{
		if(value.toUpperCase().indexOf("agent".toUpperCase())>=0)
			return true;
		else {
			return false;
		}
	},
	isSelected:function(obj)
	{
		if(obj !== "")
			return true;
		return false;

	},
	adaptCheckBoxValue:function(val)
	{
		if(_.isEmpty(val))
			return false;
		return val;
	},
  
  formatColorCreditWorthiness: function(payedOrder, totalOrder)
  {
    if(totalOrder>0)
    {
      var p = (payedOrder/totalOrder)*100;
//      var p = parseInt(orderRate);
      if(p>=60)
      {
        return "Success";
      }
      else if(p>=30 && p<60)
      {
        return "Warning";
      }
      else
      {
        return "Error";
      }
    }
    else
    {
      return "None";
    }
  },
    
  formatPercentage: function(actual, total)
  {
    if(total>0)
    {
      var p = (actual/total)*100;
      return parseInt(p);
    }else{
        return 0;
    }
        
  },
    
  formatDecimalValue: function(val){
        return val.toFixed(2);          
  },
    
  formatVisibleIcon: function(val){
    if(typeof val === "undefined"){
          return false;
      }else return true;
  },
    

    
  formatAvailable: function(value){
//      if(typeof val === "undefined"){
//          return false;
//      }else return val;
      if(value===true){
          return "./custom/img/tick.png";
      }else{
          return "./custom/img/warning.png";
      }
  },
    
  returnRowStatus: function (value) {
      if (!value) return;
        switch(value) {
            case "aperto": 
                return "None";
                break;
            case "chiuso":
                return "None";
                break;
            case "bloccato":
                return "#ff4d4d";
                break;
        }
        
    },
  
  formatValueState: function(wantedDate, futureDate){
      if(typeof wantedDate === "undefined" || typeof futureDate === "undefined") {
          return "None";
      }else if((new Date(wantedDate)) >=  (new Date(futureDate))){
          return "Success";
      }else return "None";
  },
    
  formatDateState: function(requestedDate, shippmentDate){
        var fromR = requestedDate.split("/");
        var req = new Date(fromR[2], fromR[1] - 1, fromR[0]);
        var fromS = shippmentDate.split("/");
        var shipp = new Date(fromS[2], fromS[1] - 1, fromS[0]);
        if(typeof requestedDate === "undefined" || typeof shippmentDate === "undefined") {
          return "None";
      }else if(shipp >  req){
          return "Warning";
      }else return "None";  
  },
    
//  markAsFavorite: function(marker){
//      if(marker===true){
//          return "true";
//      }else{
//          return "false";
//      }
//  },
  
  formatColorCreditDebt:  function(usedDebt, totalDebt)
  {
//    var d = parseInt(debtRate);
//    var t = parseInt(totalDebt);
    var d = (usedDebt/totalDebt)*100;
//    if(t === 0 && debtRate === 100)
//    {
//      return "Error";
//    }
    
    if(d>=80)
    {
      return "Error";
    }
    else if(d>=30 && d<80)
    {
      return "Warning";
    }
    else
    {
      return "Success";
    }
    
  },
  
  formatDateValue : function(value)
  { var res = "";
    if(value)
    {
      var d = value.split('-');
      res = d[2]+"/"+d[1]+"/"+d[0];
    }
   return res;
    
  },
  
  formatDecimal: function(value)
  {
    if (value !== undefined)
    {
      var r = parseFloat(value);
      return r.toFixed(2);
    }
    return false;
    
  }
  


};
